/* eslint-disable */
const productDetailsSelector = '.product-details-button.add-to-cart';
const productAMountSelector  = '.product-added .qty-small-number';
var didSetBoxDetails         = false;

function openBoxDetails() {
    jQuery('#body').addClass('body-overlay');
    jQuery('.box-details').addClass('is-active');

}

function closeBoxDetails() {
    jQuery('#body').removeClass('body-overlay');
    jQuery('.box-details, .product-details-information').removeClass('is-active');
    gobh_get_cart_content();
    hideAddTOCart();
}

function openCartDetails() {
    jQuery('#body').addClass('body-overlay');
    jQuery('.cart-details').addClass('is-active');
}

function closeCartDetails() {
    jQuery('#body').removeClass('body-overlay');
    jQuery('.cart-details').removeClass('is-active');
}

function showAddTOCart(){
    jQuery(productDetailsSelector).removeClass('active');
    jQuery('.product-added').addClass('active');
}

function hideAddTOCart(){
    jQuery(productDetailsSelector).addClass('active');
    jQuery('.product-added').removeClass('active');
}

function setBoxDetails(data) {
    jQuery('.product-details .product-details-image').css("background-image", "url('" + data.image_url + "')");
    jQuery('.product-details .product-details-name').text(data.name);
    jQuery('.product-details .product-details-price').html(data.price);
    jQuery('.product-details .product-details-description').html(data.short_desc);
    jQuery('.product-details .product-details-overview').html(data.description);
    jQuery('.product-details .product-details-heading').text(data.name);
    jQuery(productDetailsSelector).attr('data-productId', data.id);
    jQuery('.product-details-popup img').attr('src', `${data.image_url}`);

    setCookie('product_details_id', data.id, 1);
    didSetBoxDetails = true;
    openBoxDetails();
    const overlay = document.querySelector('.site');
    overlay.addEventListener('click', (event) => {
        if(event.target === overlay) {
            closeBoxDetails();
        }
    })
}

function get_product_details(id){
    let product_details_id = getCookie('product_details_id');
    if ( parseInt(product_details_id) === parseInt(id) && didSetBoxDetails) {
        openBoxDetails();
    } else {
        jQuery.ajax({
            type: 'post',
            url: gobh_vars.ajaxurl,
            data: {
                action: 'gobh_get_product_details',
                id: id,
            }
        }).done( function( response ) {
            var jsn = JSON.parse( response );
            if ( typeof jsn !== 'undefined' && jsn ) {
                if ( jsn.success ) {
                    setBoxDetails(jsn);
                }
            }
        }).fail( function( jqXHR ) {
            // If another HTTP 5xx error, try again...
            if ( jqXHR.status >= 500 && jqXHR.status < 600 ) {
                get_product_details(id);
            }
        });
    }
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function gobh_add_to_cart(productId, qty) {
    jQuery.ajax({
        type: 'post',
        url: gobh_vars.ajaxurl,
        data: {
            action: 'gobh_add_to_cart',
            product_id: productId,
            qty: qty,
        }
    }).done( function( response ) {
        let jsn = JSON.parse( response );
        if (typeof jsn !== 'undefined' && jsn.success) {
            jQuery(productAMountSelector).html(jsn.qty);
            showAddTOCart();
        } else {
            alert( "Something gone wrong. Please reload the page and try again." );
        }
    }).fail( function( jqXHR ) {
        // If another HTTP 5xx error, try again...
        if ( jqXHR.status >= 500 && jqXHR.status < 600 ) {
            gobh_add_to_cart(productId, qty);
        } else {
            alert( "Something gone wrong. Please reload the page and try again." );
        }
    });
}

function gobh_get_cart_content(){
    jQuery.ajax({
        type: 'post',
        url: gobh_vars.ajaxurl,
        data: {
            action: 'gobh_get_cart_content',
        }
    }).done( function( response ) {
        let cartDetails = {'total_count':0,'total_sum':'0 &euro;','subtotal':'0 &euro;'};
        if ( response.length > 0 ) {
            let jsn = JSON.parse( response );
            if (typeof jsn !== 'undefined') {
                cartDetails = jsn;
            }
        } else {
            jQuery('#cart-details-box-list').html('');
        }
        gobh_update_cart_details(cartDetails);
    }).fail( function( jqXHR ) {
        // If another HTTP 5xx error, try again...
        if ( jqXHR.status >= 500 && jqXHR.status < 600 ) {
            gobh_get_cart_content();
        }
    });
}

function gobh_update_cart_details(jsn) {
    jQuery('.header-basket-total').html(jsn.total_sum);
    jQuery('#cart-details-total').html(jsn.total_sum);
    jQuery('#cart-details-subtotal').html(jsn.subtotal);
    jQuery('.header-basket-quantity > .number').html(jsn.total_count);
}

function gobh_add_to_cart_item(productId, qty) {
    jQuery.ajax({
        type: 'post',
        url: gobh_vars.ajaxurl,
        data: {
            action: 'gobh_add_to_cart',
            product_id: productId,
            qty: qty,
        }
    }).done( function( response ) {
        let jsn = JSON.parse( response );
        if (typeof jsn !== 'undefined' && jsn.success) {
            jQuery('#product-'+productId+' .product-quantity').html(jsn.qty);
            gobh_update_cart_details(jsn);
        } else {
            alert( "Something gone wrong. Please reload the page and try again." );
        }
    }).fail( function( jqXHR ) {
        // If another HTTP 5xx error, try again...
        if ( jqXHR.status >= 500 && jqXHR.status < 600 ) {
            gobh_add_to_cart_item(productId, qty);
        } else {
            alert( "Something gone wrong. Please reload the page and try again." );
        }
    });
}

function gobh_get_cart_items() {
    jQuery.ajax({
        type: 'post',
        url: gobh_vars.ajaxurl,
        data: {
            action: 'gobh_get_cart_items'
        }
    }).done( function( response ) {
        if (response.length > 0) {
            let jsn = JSON.parse( response );
            if (typeof jsn !== 'undefined' && typeof jsn.html !== 'undefined') {
                jQuery('#cart-details-box-list').html(jsn.html);
                jQuery('.go-to-checkout').show();
                openCartDetails();
            } else {
                alert( "Something gone wrong. Please reload the page and try again." );
            }
        } else {
            jQuery('.go-to-checkout').hide();
            openCartDetails();
        }
    }).fail( function( jqXHR ) {
        // If another HTTP 5xx error, try again...
        if ( jqXHR.status >= 500 && jqXHR.status < 600 ) {
            gobh_get_cart_items();
        } else {
            alert( "Something gone wrong. Please reload the page and try again." );
        }
    });
}

jQuery(() => {
    jQuery('.content-shop-category-list li').on('click', function (event) {
        event.preventDefault();

        jQuery('.content-shop-category-list li').removeClass('active');
        jQuery(this).addClass('active');

        jQuery('html, body').animate({
            scrollTop: jQuery('#'+jQuery(this).attr('data-slug')).offset().top - 30
        }, 500);
    });

    jQuery('.content-shop-product').on('click', function(){
        if (typeof jQuery(this).attr('data-id') !== 'undefined') {
            get_product_details(jQuery(this).attr('data-id'));
        }
    });

    jQuery('.product-details-close, .product-button-close').on('click', function (){
        closeBoxDetails();
    });

    jQuery('.product-details .icon-circle-info').on('click', function (){
        jQuery('.product-details-information').addClass('is-active');
    });
    jQuery('.product-details .icon-arrow-back').on('click', function (){
        jQuery('.product-details-information').removeClass('is-active');
    });

    jQuery('.add-to-cart').on('click', function(){
        let productId = jQuery(this).attr('data-productId');
        if (typeof productId !== 'undefined') {
            gobh_add_to_cart(productId, 1);
        }
    });

    jQuery('.qty-count-more').on('click', function(){
        let productId = jQuery(productDetailsSelector).attr('data-productId');
        if (typeof productId !== 'undefined') {
            gobh_add_to_cart(productId, 1);
            jQuery(this).addClass('disabled');
            setTimeout(function ()  {
                jQuery('.qty-count-more').removeClass('disabled');
            }, 700);
        }
    });

    jQuery('.qty-count-less').on('click', function(){
        if (parseInt( jQuery(productAMountSelector).html() ) > 0) {
            let productId = jQuery(productDetailsSelector).attr('data-productId');
            if (typeof productId !== 'undefined') {
                gobh_add_to_cart(productId, -1);
                jQuery(this).addClass('disabled');
                setTimeout(function ()  {
                    jQuery('.qty-count-less').removeClass('disabled');
                }, 700);
            }
        }
    });

    jQuery(document).on('click', '.add-product', function(){
        let productId = jQuery(this).closest('.woo-product-item').attr('data-productId');
        if (typeof productId !== 'undefined') {
            gobh_add_to_cart_item(productId, 1);
        }
    });

    jQuery(document).on('click', '.remove-product', function(){
        let $parent   = jQuery(this).closest('.woo-product-item');
        let productId = $parent.attr('data-productId');
        let amount    = parseInt( $parent.find('.product-quantity').html() );

        if ( amount < 1 ) {
            $parent.remove();
        } else if ( amount === 1 ) {
            $parent.remove();
            if (typeof productId !== 'undefined') {
                gobh_add_to_cart_item(productId, -1);
            }
        } else {
            if (typeof productId !== 'undefined') {
                gobh_add_to_cart_item(productId, -1);
            }
        }
    });

    const overlay = document.querySelector('.site');
    overlay.addEventListener('click', (event) => {
        if(event.target === overlay) {
            closeCartDetails();
        }
    });

    jQuery('.header-basket').on('click', function(){
        gobh_get_cart_items();
    });

    jQuery('.cart-details-close').on('click', function(){
        closeCartDetails();
    });

    jQuery('.persons-less').on('click', function(){
        let $persons_number = jQuery('#persons_number');
        let personsCount = parseInt( $persons_number.val() );
        if ( personsCount > 1 ) {
            let newPersonsCount = personsCount - 1;
            $persons_number.val(newPersonsCount);
            jQuery('#persons_number_front').html(newPersonsCount);
        }
    });

    jQuery('.persons-more').on('click', function(){
        let $persons_number = jQuery('#persons_number');
        let newPersonsCount = parseInt( $persons_number.val() ) + 1;
        $persons_number.val(newPersonsCount);
        jQuery('#persons_number_front').html(newPersonsCount);
    });

    jQuery('#back_to_map > a').on('click', function(){
        jQuery.ajax({
            type: 'post',
            url: gobh_vars.ajaxurl,
            data: {
                action: 'gobh_clear_cart'
            }
        });
    });

});
/* eslint-enable */

const stickyHeader = {
    header: document.querySelector('.header'),
    headerHeight: 0,
    headerSticky: document.querySelector('.header-sticky'),
    lastScrollPosition: 0,
    init() {
        if (this.headerSticky) {
            this.headerHeight = this.headerSticky.offsetHeight;

            this.addHeightStyle();

            window.addEventListener('resize', () => {
                this.headerHeight = this.headerSticky.offsetHeight;
                this.addHeightStyle();
            });

            window.addEventListener('scroll', () => {

                const currentScrollPosition = window.pageYOffset;

                if (currentScrollPosition > this.lastScrollPosition && currentScrollPosition > this.headerHeight) {
                    if (!this.headerSticky.classList.contains('not-sticky')) {
                        this.headerSticky.classList.add('not-sticky');
                    }
                } else {
                    this.headerSticky.classList.remove('not-sticky');
                }
                this.lastScrollPosition = currentScrollPosition;

            });
        }
    },
    addHeightStyle() {
        this.header.style.minHeight = '';
        this.header.style.minHeight = `${this.headerHeight}px`;
    },
};

document.addEventListener('DOMContentLoaded', () => { stickyHeader.init(); });

const headerPopup = {
    popupActivationList: document.querySelectorAll('.popup-activation'),
    body: document.getElementById('body'),
    basket: document.querySelector('.header-basket-quantity .number'),
    basketState: false,
    init() {
        this.popupActivationList.forEach((popupActivation) => {
            const popup = document.querySelector(`.${popupActivation.dataset.popup}`);
            if (popup) {

                popupActivation.addEventListener('click', () => {
                    this.popupOpen(popupActivation, popup);
                });

                const closeIcon = popup.querySelector('.popup-close-icon'),
                    closeButton = popup.querySelector('.popup-close-button');

                if (closeIcon) {
                    closeIcon.addEventListener('click', () => {
                        this.popupClose(closeIcon, popup);
                    });
                }
                if (closeButton) {
                    closeButton.addEventListener('click', () => {
                        this.popupClose(closeButton, popup);
                    });
                }
            }
        });
        if (this.basket) {
            if (parseInt(this.basket.innerHTML, 10) > 0) {
                this.basketState = true;
                window.history.pushState({ page: 1 }, '', '');
            }
            this.basket.addEventListener('DOMSubtreeModified', () => {
                if (!this.basketState) {
                    this.basketState = true;
                    window.history.pushState({ page: 1 }, '', '');
                }
            });
            window.onpopstate = (event) => {
                if (event) {
                    const addressPopupActivation = document.querySelector('.header-address'),
                        addressPopup = document.querySelector('.address-popup');
                    this.popupOpen(addressPopupActivation, addressPopup);
                }
            };
        }
        const agePopupElement = document.querySelector('.age-popup');
        if (agePopupElement) {
            this.agePopup(agePopupElement);
        }
    },
    popupOpen(popupActivation, popup) {
        if (popupActivation.dataset.popup === 'address-popup') {

            if (parseInt(this.basket.innerHTML, 10) > 0) {
                popup.classList.add('is-active');
                this.body.classList.add('body-overlay');
            } else {
                window.location.href = jQuery('#back_to_map > a').attr('href');
            }
        } else {
            popup.classList.add('is-active');
            this.body.classList.add('body-overlay');
        }
    },
    popupClose(closeElement, popup) {
        if (popup.classList.contains('address-popup')) {
            window.history.pushState({ page: 1 }, '', '');
            popup.classList.remove('is-active');
            this.body.classList.remove('body-overlay');
        } else {
            popup.classList.remove('is-active');
            this.body.classList.remove('body-overlay');
        }
    },
    agePopup(popup) {
        popup.classList.add('is-active');
        this.body.classList.add('body-overlay');
        const closeButton = popup.querySelector('.popup-close-button');
        if (closeButton) {
            closeButton.addEventListener('click', () => {
                this.popupClose(closeButton, popup);
            });
        }
    },
};

document.addEventListener('DOMContentLoaded', () => { headerPopup.init(); });

const productDetailsPopup = {
    popupActivation: document.querySelector('.product-details-image'),
    popupBlock: document.querySelector('.product-details-popup'),
    popupBlockClose: document.querySelector('.product-details-popup .product-details-popup-close'),
    init() {
        if (this.popupActivation) {
            this.popupActivation.addEventListener('click', (event) => {
                if (event.target === this.popupActivation) {
                    this.openPopup();
                }
            });
            this.popupBlock.addEventListener('click', (event) => {
                if (event.target === this.popupBlock || event.target === this.popupBlockClose) {
                    this.closePopup();
                }
            });
        }
    },
    openPopup() {
        this.popupBlock.style.display = 'block';
    },
    closePopup() {
        this.popupBlock.style.display = '';
    },
};

document.addEventListener('DOMContentLoaded', () => { productDetailsPopup.init(); });

const orderFormValidation = {
    orderForm: document.querySelector('.order-box-form'),
    init() {
        if (this.orderForm) {
            const inputName = this.orderForm.querySelector('#client-name'),
                inputEmail = this.orderForm.querySelector('#client-email'),
                inputPhone = this.orderForm.querySelector('#client-phone');

            this.orderForm.addEventListener('submit', (e) => {
                e.preventDefault();

                this.checkRequired(inputName);
                this.checkRequired(inputPhone);
                this.checkEmail(inputEmail);
                if (!inputName.closest('.order-box-info-text').classList.contains('is-error')
                    && !inputEmail.closest('.order-box-info-text').classList.contains('is-error')
                    && !inputPhone.closest('.order-box-info-text').classList.contains('is-error')) {
                    this.orderForm.submit();
                }
            });
        }
    },
    checkRequired(input) {
        if (input.value.trim() === '') {
            this.showError(input);
        } else {
            this.showSuccess(input);
        }
    },
    checkEmail(input) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(input.value.trim())) {
            this.showSuccess(input);
        } else {
            this.showError(input);
        }
    },
    showError(input) {
        const inputControl = input.closest('.order-box-info-text');
        inputControl.classList.add('is-error');
    },
    showSuccess(input) {
        const inputControl = input.closest('.order-box-info-text');
        inputControl.classList.remove('is-error');
    },
};

document.addEventListener('DOMContentLoaded', () => { orderFormValidation.init(); });
