<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package go_by_henssler
 */

$cart_content = gobh_get_cart_content();
$items_count  = ( ! empty( $cart_content['total_count'] ) ) ? $cart_content['total_count'] : 0;
$total_sum    = ( ! empty( $cart_content['total_sum'] ) ) ? $cart_content['total_sum'] : '0 &euro;';
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_theme_file_uri( 'dist/img/favicon/apple-touch-icon.png' ) ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_theme_file_uri( 'dist/img/favicon/favicon-32x32.png' ) ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_theme_file_uri( 'dist/img/favicon/favicon-16x16.png' ) ?>">
    <link rel="manifest" href="<?php echo get_theme_file_uri( 'dist/img/favicon/site.webmanifest' ) ?>">
    <link rel="mask-icon" href="<?php echo get_theme_file_uri( 'dist/img/favicon/safari-pinned-tab.svg' )?>" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <?php wp_head(); ?>
</head>

<body id="body" <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
