<?php
    $ids = gobh_get_category_products( $args['cat']->slug, $args['product_list'], $args['exclude_list'] );
?>
<div class="content-shop-category">
    <h2 id="<?php echo 'category-' . esc_html( $args['cat']->slug );?>" class="h1-style">
        <?php echo esc_html( $args['cat']->name ); ?>
    </h2>
    <div class="content-shop-product-list">
        <?php
            foreach ( $ids as $id ) {
                $product = wc_get_product( $id );

                if ( ! is_wp_error( $product ) && $product ) {
                    get_template_part( 'template-parts/content', 'shop-product', $product );
                }
            }
        ?>
    </div>
</div>