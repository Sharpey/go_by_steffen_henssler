<?php
$image     = wp_get_attachment_image_src( get_post_thumbnail_id( $args->id ), 'full' );
$image_src = '';

if ( ! empty( $image[0] ) ) {
    $image_src = $image[0];
} else {
    $image_src = gobh_get_blank_image_src();
}

$short_description = esc_html( strip_tags( $args->get_short_description() ) );
if ( empty( $short_description ) ) {
    $short_description = gobh_product_short_description_fallback();
}
?>
<div class="content-shop-product-item">
    <div class="content-shop-product" data-id="<?php echo esc_attr( $args->id ); ?>">
        <div class="content-shop-product-image">
            <img src="<?php echo $image_src; ?>" />
            <div class="add-to-cart content-shop-product-add" data-productId="<?php echo esc_attr( $args->id ); ?>">
                <span class="icon-circle-plus"></span>
            </div>
        </div>
        <div class="content-shop-product-info">
            <span class="content-shop-product-name"><?php echo esc_html( $args->get_name() ); ?></span>
            <span class="content-shop-product-description"><?php echo gobh_form_short_description( $short_description ); ?></span>
            <span class="content-shop-product-price"><?php echo esc_html( $args->get_price() ); ?> <?php echo get_woocommerce_currency_symbol(); ?></span>
        </div>
    </div>
</div>