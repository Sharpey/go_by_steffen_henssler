<li class="order-box-item woo-product-item"
    data-productId="<?php echo esc_attr( $args['product_id'] ); ?>"
    id="product-<?php echo esc_attr( $args['product_id'] ); ?>"
>
    <div class="product-image">
        <img src="<?php echo esc_attr( $args['image_url'] ); ?>" alt="<?php echo esc_attr( $args['name'] ); ?>">
    </div>
    <div class="product-info">
        <span class="product-quantity"><?php echo esc_html( $args['qty'] ); ?></span>
        <div class="product-more">
            <span class="product-title"><?php echo esc_html( $args['name'] ); ?></span>
            <span class="product-description"><?php echo esc_html( $args['short_description'] ); ?></span>
        </div>
    </div>
    <div class="product-data">
        <span class="product-price"><?php echo esc_html( $args['price'] ); ?></span>
        <div class="qty-count">
            <span class="qty-count-less icon-circle-minus remove-product"></span>
            <span class="qty-count-more icon-circle-plus add-product"></span>
        </div>
    </div>
</li>