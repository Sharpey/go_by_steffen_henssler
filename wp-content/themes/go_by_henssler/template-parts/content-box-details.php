
<div class="product-details box-details">
    <div class="product-details-image">
        <span class="product-details-close icon-cross"></span>
    </div>
    <div class="product-details-box">
        <div class="product-details-title">
            <span class="product-details-name"></span>
            <span class="icon-circle-info"></span>
        </div>
        <span class="product-details-description"></span>
        <span class="product-details-price"></span>
        <span class="btn product-details-button add-to-cart active">
            <?php _e( 'Zur Bestellung hinzufügen', 'go_by_henssler' ); ?>
        </span>

        <div class="product-added qty-small">
            <div class="qty-small-box bg-gray">
                <div class="qty-small-info">
                    <span class="qty-small-label">
                        <?php _e( 'Anzahl', 'go_by_henssler' ); ?>:
                    </span>
                    <span class="qty-small-number"></span>
                </div>
                <div class="qty-small-count qty-count">
                    <span class="qty-count-less icon-circle-minus"></span>
                    <span class="qty-count-more icon-circle-plus"></span>
                </div>
            </div>
        </div>

        <span class="btn btn-border product-details-button product-button-close product-added">
            <?php _e( 'zurück zur Übersicht', 'go_by_henssler' ); ?>
        </span>
    </div>

    <div class="product-details-information">
        <div class="product-details-box">
            <div class="product-details-header">
                <span class="icon-arrow-back"></span>
                <span class="product-details-heading"></span>
                <span class="icon-cross product-button-close"></span>
            </div>
            <div class="product-details-overview"></div>
        </div>
    </div>
</div>
