<ul class="content-shop-category-list">
    <?php
    $active = 'active';
    foreach ( $args as $cat ) : ?>
        <li
            class="content-shop-category-item <?php echo $active; ?>"
            data-id="<?php echo esc_html( $cat->term_id ); ?>"
            data-slug="<?php echo 'category-' . esc_html( $cat->slug ); ?>">
            <?php echo esc_html( $cat->name ); ?>
        </li>
        <?php
        $active = '';
    endforeach;
    ?>
</ul>