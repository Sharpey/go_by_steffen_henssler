<?php
    $cart_totals = gobh_get_cart_content();
    $subtotal  = ( ! empty( $cart_totals['subtotal'] ) ) ? $cart_totals['subtotal'] : '0 &euro;';
    $total_sum = ( ! empty( $cart_totals['total_sum'] ) ) ? $cart_totals['total_sum'] : '0 &euro;';
?>
<div class="cart-details product-details">
    <div class="cart-details-head">
        <span class="cart-details-title">
            <?php _e( 'Warenkorb', 'go_by_henssler' ); ?>
        </span>
        <span class="cart-details-close icon-cross"></span>
    </div>

    <div class="cart-details-box">
        <span class="cart-details-box-subtitle">
            <?php _e( 'Produktauswahl', 'go_by_henssler' ); ?>
        </span>
        <ul class="cart-details-box-list" id="cart-details-box-list"><?php echo gobh_get_cart_details(); ?></ul>
    </div>

    <div class="order-box-sum">
        <div class="order-box-subtotal">
            <span class="title">
                <?php _e( 'Zwischensumme', 'go_by_henssler' ); ?>
            </span>
            <span id="cart-details-subtotal" class="price"><?php echo esc_html( $subtotal ); ?></span>
        </div>
        <div class="order-box-total">
            <span class="title">
                <?php _e( 'Gesamt', 'go_by_henssler' ); ?>
            </span>
            <span id="cart-details-total" class="price"><?php echo esc_html( $total_sum ); ?></span>
        </div>
    </div>

    <div class="cart-details-footer">
        <a class="btn go-to-checkout" href="<?php echo esc_url( wc_get_checkout_url() ); ?>">
            <?php _e( 'zur Kasse gehen', 'go_by_henssler' ); ?>
        </a>
    </div>
</div>
