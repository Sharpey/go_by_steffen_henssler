<ul class="footer-nav">
    <?php foreach ( $args as $menu_item ): ?>
    <li
        <?php if ( ! empty( $menu_item->classes[0] ) ) { echo ' class="' . esc_attr( implode( ' ', $menu_item->classes ) ) . '"'; } ?>
    >
        <a
            <?php if ( ! empty( $menu_item->attr_title ) ) { echo ' title="' . esc_attr( $menu_item->attr_title ) . '"'; } ?>
            <?php if ( ! empty( $menu_item->xfn ) ) { echo ' rel="' . esc_attr( $menu_item->xfn ) . '"'; } ?>
            <?php if ( ! empty( $menu_item->target ) ) { echo ' target="' . esc_attr( $menu_item->target ) . '"'; } ?>
            href="<?php echo esc_url( $menu_item->url ); ?>"
        ><?php echo esc_html( $menu_item->title ); ?></a>
    </li>
    <?php endforeach; ?>
</ul>