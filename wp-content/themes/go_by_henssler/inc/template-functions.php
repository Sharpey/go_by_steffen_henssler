<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package go_by_henssler
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function gobh_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'gobh_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function gobh_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'gobh_pingback_header' );

/**
 * Load google fonts in efficient way
 */
function gobh_load_fonts() {

    $icon_font = 'go_by_henssler';
    $avenir_font = 'avenir';
    $sourcesans_font = 'sourcesans';

    /**
     * https://csswizardry.com/2020/05/the-fastest-google-fonts/
     */
    ?>
    <style type="text/css" id="avenir-font">
        @font-face {
            font-family: '<?php echo $avenir_font; ?>';
            src: url('<?php echo get_template_directory_uri(); ?>/dist/fonts/<?php echo $avenir_font; ?>.ttf') format('truetype'),
            url('<?php echo get_template_directory_uri(); ?>/dist/fonts/<?php echo $avenir_font; ?>.woff') format('woff'),
            url('<?php echo get_template_directory_uri(); ?>/dist/fonts/<?php echo $avenir_font; ?>.svg?#<?php echo $icon_font; ?>') format('svg');
            font-weight: normal;
            font-style: normal;
            font-display: block;
        }
    </style>
    <style type="text/css" id="sourcesans-font">
        @font-face {
            font-family: '<?php echo $sourcesans_font; ?>';
            src: url('<?php echo get_template_directory_uri(); ?>/dist/fonts/<?php echo $sourcesans_font; ?>.ttf') format('truetype'),
            url('<?php echo get_template_directory_uri(); ?>/dist/fonts/<?php echo $sourcesans_font; ?>.woff') format('woff'),
            url('<?php echo get_template_directory_uri(); ?>/dist/fonts/<?php echo $sourcesans_font; ?>.svg?#<?php echo $icon_font; ?>') format('svg');
            font-weight: 300;
            font-style: normal;
            font-display: block;
        }
    </style>
    <style type="text/css" id="go_by_henssler-font">
        @font-face {
            font-family: '<?php echo $icon_font; ?>';
            src: url('<?php echo get_template_directory_uri(); ?>/dist/fonts/<?php echo $icon_font; ?>.ttf') format('truetype'),
            url('<?php echo get_template_directory_uri(); ?>/dist/fonts/<?php echo $icon_font; ?>.woff') format('woff'),
            url('<?php echo get_template_directory_uri(); ?>/dist/fonts/<?php echo $icon_font; ?>.svg?#<?php echo $icon_font; ?>') format('svg');
            font-weight: normal;
            font-style: normal;
            font-display: block;
        }
    </style>
    <?php
}
add_action( 'wp_head', 'gobh_load_fonts' );

/**
 * Get list of WooCommerce product categories
 *
 * @param array $product_list
 * @return false|int[]|string|string[]|WP_Error|WP_Term[]
 */
function gobh_get_product_categories( $product_list = [] ) {
    $taxonomy   = 'product_cat';
    $terms_list = [];

    $args = array(
        'taxonomy'     => $taxonomy,
        'orderby'      => 'name',
        'order'        => 'DESC',
        'show_count'   => 0,
        'pad_counts'   => 0,
        'hierarchical' => 0,
        'title_li'     => '',
        'hide_empty'   => 1
    );

    if ( ! empty( $product_list ) ) {
        foreach ( $product_list as $product_id ) {
            if ( empty( $_SESSION['old_enough'] ) ) {
                if ( has_term( 'alcohol', 'product_tag', $product_id ) ) {
                    continue;
                }
            }

            $product_terms = wp_get_post_terms( $product_id, $taxonomy, ['fields' => 'ids'] );

            foreach ( $product_terms as $term ) {
                if ( ! empty( $term ) && ! in_array( $term, $terms_list ) ) {
                    $terms_list[] = $term;
                }
            }
        }
    }

    if ( ! empty( $terms_list ) ) {
        $args['include'] = $terms_list;
    }

    $all_categories = get_terms( $args );

    if ( ! is_wp_error( $all_categories ) && $all_categories ) {
        return $all_categories;
    }

    return false;
}

/**
 * Get products ids by category slug
 *
 * @param $category_slug
 * @param $product_list - get only included products
 * @param $exclude - but exclude these products from results
 * @return false|int[]|WP_Post[]
 */
function gobh_get_category_products( $category_slug, $product_list, $exclude ) {

    $args = [
        'post_type'   => 'product',
        'post_status' => 'publish',
        'fields'      => 'ids',
        'numberposts' => -1,
        'tax_query'   => [
            [
                'taxonomy' => 'product_cat',
                'field'    => 'slug',
                'terms'    => $category_slug,
                'operator' => 'IN',
            ],
        ],
    ];

    if ( ! empty( $exclude ) && ! empty( $product_list ) ) {
        $product_list = array_diff( $product_list, $exclude );
    } elseif ( ! empty( $exclude ) ) {
        $args['post__not_in'] = $exclude;
    }

    if ( ! empty( $product_list ) ) {
        $args['post__in'] = $product_list;
    }

    $all_products = get_posts( $args );

    if ( ! is_wp_error( $all_products ) && $all_products ) {
        return $all_products;
    }

    return false;
}

/**
 * Get blank image
 * @return string base64 of image
 */
function gobh_get_blank_image_src(){
    return 'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';
}

/**
 * Make very short description
 * @return string
 */
function gobh_form_short_description( $string, $symbols = 21, $after = ' ...' ){
    return mb_substr( strip_tags( $string ), 0, $symbols ) . $after;
}

/**
 * Define and outputs in header global JS values
 * This function should perform what we have been using wp_localize_script for
 *
 * @return mixed JS
 */
function gobh_inline_global_js() {

    $js_var = 'gobh_vars';
    $var_items = [
        'themeurl'    => get_template_directory_uri(),
        'ajaxurl'     => admin_url( 'admin-ajax.php' ),
        'gobh_nonce'  => wp_create_nonce( 'gobh-nonce' ),
    ];

    foreach ( $var_items as $key => $value ) {

        if ( ! is_scalar( $value ) ) {
            continue;
        }
        $var_items[$key] = html_entity_decode( (string) $value, ENT_QUOTES, 'UTF-8' );
    }

    $script = "var $js_var = " . wp_json_encode( $var_items ) . ';';
    ?>
    <script type="text/javascript" id="gobh_vars">
        <?php echo $script; ?>
    </script>
    <?php
}
add_action( 'wp_head', 'gobh_inline_global_js', 10, 1 );

/**
 * Callback for `gobh_get_product_details` ajax action
 * Returns data for 'Box details` by product id
 */
function gobh_get_product_details() {
    $id   = ( ! empty( $_POST['id'] ) ) ? intval( $_POST['id'] ) : 0;
    $data = new \stdClass();
    $data->success = false;

    if ( ! empty( $id ) ) {
        $product = wc_get_product( $id );

        if ( $product ) {
            $data->id          = esc_html( $id );
            $data->name        = esc_html( $product->get_name() );
            $data->price       = esc_html( $product->get_price() . ' ' . get_woocommerce_currency_symbol() );
            $data->description = wp_kses_post( $product->get_description() );
            $data->short_desc  = wp_kses_post( $product->get_short_description() );
            $image_url         = wp_get_attachment_image_url( get_post_thumbnail_id( $id ), 'full' );
            $data->image_url   = ( ! empty( $image_url ) ) ? $image_url : gobh_get_blank_image_src();

            if ( empty( $data->short_desc ) ) {
                $data->short_desc = gobh_product_short_description_fallback();
            }
        }
    }

    if ( ! empty( $data->name ) && ! empty( $data->price ) ) {
        $data->success = true;
    }

    echo json_encode( $data );

    wp_die();
}
add_action( 'wp_ajax_gobh_get_product_details', 'gobh_get_product_details' );
add_action( 'wp_ajax_nopriv_gobh_get_product_details', 'gobh_get_product_details' );

/**
 * Output footer menu
 */
function gobh_output_footer_menu() {

    $menu_items = wp_get_nav_menu_items( 'footer-menu' );

    if ( empty( $menu_items ) ) {
        return;
    }

    get_template_part( 'template-parts/footer', 'menu', $menu_items );
}

/**
 * Callback function for add to cart ajax action
 */
function gobh_add_to_cart_callback() {
    $product_id = intval( $_POST['product_id'] );
    $qty        = intval( $_POST['qty'] );
    $response   = new \stdClass();
    $response->success = false;
    $response->message = 'wrong quantity';

    if ( $qty == 1 ) {
        try {
            $cart_item_key = WC()->cart->add_to_cart( $product_id, $qty );

            if ( $cart_item_key ) {
                $response->success = true;
            } else {
                $response->message = $cart_item_key;
            }

            $cart_item = WC()->cart->get_cart_item( $cart_item_key );

            if ( $cart_item ) {
                $response->qty = intval( $cart_item['quantity'] );
            }
        }
        catch ( Exception $exc ) {
            $response->message = $exc->getMessage();
        }
    } else if ( $qty == -1 ) {
        $cart_item_key = '';
        $cart_items    = WC()->cart->get_cart();

        if ( ! empty( $cart_items ) ) {
            foreach ( $cart_items as $cart_item ) {
                if ( $cart_item['product_id'] == $product_id ) {
                    $cart_item_key = $cart_item['key'];
                    break;
                }
            }
        }

        if ( ! empty( $cart_item_key ) ) {
            $cart_item = WC()->cart->get_cart_item( $cart_item_key );

            if ( $cart_item['quantity'] == 1 ) {
                if ( WC()->cart->remove_cart_item( $cart_item_key ) ) {
                    $response->success = true;
                    $response->qty     = 0;
                }
            } else {
                if ( WC()->cart->set_quantity( $cart_item_key, $cart_item['quantity'] + $qty, true ) ) {
                    $response->success = true;
                    $response->qty     = $cart_item['quantity'] + $qty;
                }
            }
        }
    }

    $cart_details = gobh_get_cart_content();

    if ( ! empty( $cart_details ) ) {
        $response->subtotal    = $cart_details['subtotal'];
        $response->total_sum   = $cart_details['total_sum'];
        $response->total_count = $cart_details['total_count'];
    } else {
        $response->subtotal    = '0 &euro;';
        $response->total_sum   = '0 &euro;';
        $response->total_count = 0;
    }

    echo json_encode( $response );
    wp_die();
}
add_action( 'wp_ajax_gobh_add_to_cart', 'gobh_add_to_cart_callback' );
add_action( 'wp_ajax_nopriv_gobh_add_to_cart', 'gobh_add_to_cart_callback' );

/**
 * Get total sum and cart total items count for cart icon in header
 *
 * @return array|false
 */
function gobh_get_cart_content() {
    $totals     = WC()->cart->get_totals();
    $woo_symbol = get_woocommerce_currency_symbol();

    if ( ! empty( $totals['total'] ) ) {
        return [
            'subtotal'    => $totals['total'] . ' ' . $woo_symbol,
            'total_sum'   => $totals['total'] . ' ' . $woo_symbol,
            'total_count' => WC()->cart->get_cart_contents_count(),
        ];
    }

    return false;
}

/**
 * Callback for ajax call. Will return total sum and total items count in cart
 */
function gobh_get_cart_content_callback() {

    $response = gobh_get_cart_content();

    if ( ! empty( $response ) ) {
        echo json_encode( $response );
    }

    wp_die();
}
add_action( 'wp_ajax_gobh_get_cart_content', 'gobh_get_cart_content_callback' );
add_action( 'wp_ajax_nopriv_gobh_get_cart_content', 'gobh_get_cart_content_callback' );

/**
 * Get html of cart details items
 *
 * @param bool $include_images
 * @return false|string
 */
function gobh_get_cart_details( $include_images = false ) {
    $cart = WC()->cart->get_cart();

    if ( ! empty( $cart ) ) {
        ob_start();
        foreach ( $cart as $item ) {
            $data = [
                'product_id'        => $item['product_id'],
                'qty'               => $item['quantity'],
                'name'              => $item['data']->get_name(),
                'price'             => number_format($item['data']->get_price(), 2, ',', '') . ' ' . get_woocommerce_currency_symbol(),
                'short_description' => $item['data']->get_short_description(),
            ];

            if ( empty( $data['short_description'] ) ) {
                $data['short_description'] = gobh_product_short_description_fallback();
            }

            $data['short_description'] = gobh_form_short_description( $data['short_description'] );

            if ( $include_images ) {
                $image_url         = wp_get_attachment_image_url( get_post_thumbnail_id( $item['product_id'] ), 'full' );
                $data['image_url'] = ( ! empty( $image_url ) ) ? $image_url : gobh_get_blank_image_src();

                get_template_part( 'template-parts/checkout-product', 'item', $data );
            } else {

                get_template_part( 'template-parts/cart-details', 'item', $data );
            }
        }
    }

    return ob_get_clean();
}

/**
 * Callback for ajax call. Will return html of cart items for cart details box
 */
function gobh_get_cart_items_html() {

    $response = gobh_get_cart_details();

    if ( ! empty( $response ) ) {
        echo json_encode( ['html' => $response] );
    }

    wp_die();
}
add_action( 'wp_ajax_gobh_get_cart_items', 'gobh_get_cart_items_html' );
add_action( 'wp_ajax_nopriv_gobh_get_cart_items', 'gobh_get_cart_items_html' );

/**
 * Callback for ajax call. Will remove everything from the cart.
 */
function gobh_clear_cart() {

    WC()->cart->empty_cart();

    wp_die();
}
add_action( 'wp_ajax_gobh_clear_cart', 'gobh_clear_cart' );
add_action( 'wp_ajax_nopriv_gobh_clear_cart', 'gobh_clear_cart' );

/**
 * Get all subcompany locations along with their tours locations
 *
 * @return array
 */
function gobh_get_map_locations() {
    $date_time     = new DateTime();
    $time_zone     = new DateTimeZone( 'CET' );
    $current_date  = $date_time->setTimezone( $time_zone )->format( 'Ymd' );
    $current_time  = $date_time->setTimezone( $time_zone )->format( 'Hi' );
    $cur_timestamp = $date_time->setTimezone( $time_zone )->format( 'U' );
    $subcompanies  = get_posts([
        'post_type'   => 'subcompany',
        'numberposts' => -1,
    ]);
    $addresses = $locations = [];

    if ( ! empty( $subcompanies ) ) {
        foreach ( $subcompanies as $subcompany ) {
            $meta = get_post_meta( $subcompany->ID );

            // Get this subcompany location
            $location           = new \stdClass();
            $location->title    = $subcompany->post_title;
            $location->url      = get_permalink( $subcompany );
            $location->lon      = $meta['location_lon'][0] ?? '';
            $location->lat      = $meta['location_lat'][0] ?? '';
            $location->address  = $meta['full_address'][0] ?? '';
            $location->place_id = uniqid( $location->title . $location->address );
            $location->color    = 'black';

            // Get this subcompany tours locations
            $location->location_tours = [];
            $number_tours = $meta['tour'][0];

            for ( $i = 0; $i < $number_tours; $i++ ) {
                $active = $meta['tour_'.$i.'_active'][0] ?? false;

                if ( ! $active ) {
                    continue;
                }

                $tour_date = $meta['tour_'.$i.'_date'][0] ?? '';

                if ( $tour_date > $current_date ) {
                    $tour_time = ! empty( $meta['tour_'.$i.'_time_to_accept_orders'][0] ) ? $meta['tour_'.$i.'_time_to_accept_orders'][0] : '23:59:00';
                    $tour_time = str_replace( ':', '', substr( $tour_time, 0, -3 ) );
                    /* If tour date is bigger than current date by only one day check the max time for order placement */
                    if ( ( strtotime( $tour_date, $cur_timestamp ) - strtotime( $current_date, $cur_timestamp ) ) / 60 / 60 / 24 === 1 ) {
                        /* If current local time by CET is surpassed the time to make an order for the tour - do not show it on the map */
                        if ( $tour_time <= $current_time ) {
                            continue;
                        }
                    }

                    $tour_address = $meta['tour_'.$i.'_address'][0] ?? '';
                    /* If tours have same address show only closest tour.
                    This avoids error when map icons overlaping and we have no controll of what tour will be clickable */
                    if ( key_exists( $tour_address, $addresses ) ) {
                        if ( $addresses[$tour_address]['date'] < $tour_date ) {
                            continue;
                        } else {
                            unset( $location->location_tours[$addresses[$tour_address]['id']] );
                        }
                    }

                    $addresses[$tour_address] = [
                        'id'   => $i,
                        'date' => $tour_date,
                    ];

                    $tour = new \stdClass();
                    $tour->address    = $tour_address;
                    $tour->title      = $tour->address;
                    $tour->date       = $tour_date;
                    $tour->start_time = $meta['tour_'.$i.'_start_time'][0] ?? '';
                    $tour->end_time   = $meta['tour_'.$i.'_end_time'][0] ?? '';
                    $tour->lon        = $meta['tour_'.$i.'_longitude'][0] ?? '';
                    $tour->lat        = $meta['tour_'.$i.'_latitude'][0] ?? '';
                    $tour->url        = $location->url . '?tour=' . $i;
                    $tour->place_id   = uniqid( $tour->address . $tour->date );
                    $tour->color      = 'red';

                    $location->location_tours[$i] = $tour;
                }
            }

            $locations[] = $location;
        }
    }

    return $locations;
}

/**
 * Convert date of a tour from Ymd format into designed format
 *
 * @param $tour_date
 * @return string
 */
function gobh_convert_tour_date( $tour_date ) {
    $timestamp = DateTime::createFromFormat( 'Ymd', $tour_date )->getTimestamp();

    $day_of_week = date( 'N', $timestamp );
    $week = [
        1 => 'Mo.',
        2 => 'Di.',
        3 => 'Mi.',
        4 => 'Do.',
        5 => 'Fr.',
        6 => 'Sa.',
        7 => 'So.',
    ];

    return $week[$day_of_week] . ', ' . date( 'd.m.Y', $timestamp );
}

/**
 * Get available enabled payment methods
 *
 * @return array
 */
function gobh_get_available_payments() {
    $gateways         = WC()->payment_gateways->get_available_payment_gateways();
    $enabled_gateways = [];

    if ( $gateways ) {
        foreach ( $gateways as $key => $gateway ) {
            if ( $gateway->enabled == 'yes' ) {
                $enabled_gateways[$key] = $gateway;
            }
        }
    }

    return $enabled_gateways;
}

/**
 * Add meta box to order sidebar
 *
 * @return void
 */
function gobh_add_custom_meta_box() {
    add_meta_box(
        'gobh_order_custom_meta_box',
        'Tour',
        'gobh_order_custom_meta_box_content',
        'shop_order',
        'side',
        'default'
    );
}
add_action('add_meta_boxes', 'gobh_add_custom_meta_box');

/**
 * Content to show on orders sidebar metabox
 *
 * @return void
 */
function gobh_order_custom_meta_box_content() {
    global $post;

    $subcompany_id = get_post_meta( $post->ID, 'subcompany', true );
    $tour_id       = get_post_meta( $post->ID, 'tour', true );

    ?>
        <p><b><?php _e( 'Anzahl der personen', 'go_by_henssler' ); ?>:</b> <?php echo get_post_meta( $post->ID, 'anzahl', true ); ?></p>
    <?php

    if ( ! empty( $subcompany_id ) ) :
        $subcompany = get_post( $subcompany_id );
        if ( ! empty( $subcompany ) && ! is_wp_error( $subcompany ) ) :
        ?>
        <p><b><?php _e( 'Subcompany', 'go_by_henssler' ); ?>:</b>
            <a href="<?php echo esc_url( get_edit_post_link( $subcompany->ID ) ); ?>" target="_blank"><?php echo esc_html( $subcompany->post_title ); ?></a></p>
        <?php
            $details = gobh_get_tour_details_dashboard( $subcompany->ID, $tour_id );
        ?>
            <p>
                <b><?php _e( 'Tour', 'go_by_henssler' ); ?>:</b><br>
                <?php echo $details; ?>
            </p>
        <?php
        endif;
    endif;
}

/**
 * Override smtp settings by subcompany smtp settings data
 *
 * @param $options
 * @return array
 */
function gobh_custom_smtp_settings( $options ){

    if ( ! empty( $_SESSION['subcompany'] ) ) {
        $meta = get_post_meta( $_SESSION['subcompany'] );

        if ( ! empty( $meta['smtp_host'][0] ) ) {
            $options['mail']['host'] = $meta['smtp_host'][0];
            $options['mail']['mailer'] = 'smtp';
            $options['smtp']['autotls'] = true;
            $options['smtp']['encryption'] = 'tls';
        }

        if ( ! empty( $meta['from_email'][0] ) ) {
            $options['mail']['from_email'] = $meta['from_email'][0];
            $options['mail']['from_email_force'] = true;
        }

        if ( ! empty( $meta['from_name'][0] ) ) {
            $options['mail']['from_name'] = $meta['from_name'][0];
        }

        if ( ! empty( $meta['smtp_port'][0] ) ) {
            $options['mail']['port'] = $meta['smtp_port'][0];
        }

        if ( ! empty( $meta['authentication'][0] ) ) {
            $options['smtp']['auth'] = true;
            $options['smtp']['user'] = $meta['smtp_username'][0];
            $options['smtp']['pass'] = $meta['smtp_password'][0];
        } else {
            $options['smtp']['auth'] = false;
        }
    }

    return $options;
}
add_filter( 'wp_mail_smtp_populate_options', 'gobh_custom_smtp_settings' );

/**
 * Limit subcompany and orders visibility
 *
 * @return void
 */
function gobh_subcompany_editor_user(){
    $user = wp_get_current_user();

    if ( is_admin() && ! in_array( 'administrator', $user->roles ) ) {
        add_action( 'pre_get_posts', 'gobh_filter_query_for_subcompany_manager', 1 );
    }
}
add_action( 'wp_loaded', 'gobh_subcompany_editor_user' );

/**
 * If user has assigned subcompany - show only that subcompany and only orders related to that subcompany
 *
 * @param $query
 * @return void
 */
function gobh_filter_query_for_subcompany_manager( $query ) {

    $query_post_type = $query->get( 'post_type' );

    if ( $query_post_type === 'subcompany' ) {
        $user = wp_get_current_user();
        $user_subcompany = get_user_meta( $user->ID, 'assigned_subcompany', true );

        if ( ! empty( $user_subcompany ) ) {
            $query->set( 'post__in', [$user_subcompany] );
        }
    } else if ( $query_post_type === 'shop_order' ) {
        $user = wp_get_current_user();
        $user_subcompany = get_user_meta( $user->ID, 'assigned_subcompany', true );

        if ( ! empty( $user_subcompany ) ) {
            $query->set( 'meta_key', 'subcompany' );
            $query->set( 'meta_value', $user_subcompany );
        }
    }
}

/**
 * Hide some products if user is not old enough for them
 *
 * @return void
 */
function gobh_age_restriction() {
    $old_enough = $_SESSION['old_enough'] ?? false;

    if ( ! is_admin() && ! $old_enough ) {
        add_action( 'pre_get_posts', 'gobh_hide_alcohol_products', 1 );
    }
}
add_action( 'wp_head', 'gobh_age_restriction' );

/**
 * Modify product query to hide products with alcohol tag
 *
 * @param $query
 * @return void
 */
function gobh_hide_alcohol_products( $query ) {
    if ( $query->get( 'post_type' ) === 'product' ) {
        $tax_query     = $query->get( 'tax_query' );
        $new_tax_query = [
            'taxonomy' => 'product_tag',
            'field'    => 'slug',
            'operator' => 'NOT IN',
            'terms'    => ['alcohol'],
        ];

        if ( empty( $tax_query ) ) {
            $tax_query = [];
        }

        $tax_query[] = $new_tax_query;

        $query->set( 'tax_query', $tax_query );
    }
}

/**
 * Form details of a tour to display in dashboard
 *
 * @param $subcompany_id
 * @param $tour_id
 * @return string
 */
function gobh_get_tour_details_dashboard( $subcompany_id, $tour_id ) {

    $details       = '';
    $post_meta     = (object)get_post_meta( $subcompany_id );
    $tour          = new \stdClass();
    $tour->address = ( ! empty( $post_meta->{'tour_' . $tour_id . '_address'}[0] ) ) ? $post_meta->{'tour_' . $tour_id . '_address'}[0] : '';

    if ( ! empty( $tour->address ) ) {
        $tour->date    = ( ! empty( $post_meta->{'tour_' . $tour_id . '_date'}[0] ) ) ? gobh_convert_tour_date( $post_meta->{'tour_' . $tour_id . '_date'}[0] ) : '';
        $tour_start_hr = ( ! empty( $post_meta->{'tour_' . $tour_id . '_start_time'}[0] ) ) ? $post_meta->{'tour_' . $tour_id . '_start_time'}[0] : '';
        $tour_end_hr   = ( ! empty( $post_meta->{'tour_' . $tour_id . '_end_time'}[0] ) ) ? $post_meta->{'tour_' . $tour_id . '_end_time'}[0] : '';
        $tour->time    = substr( esc_html( $tour_start_hr ), 0, -3 ) . '-' . substr( esc_html( $tour_end_hr ), 0, -3 );

        $details = esc_html( $tour->address ) . '</br>' . esc_html( $tour->date ) . '</br>' . esc_html( $tour->time ) . ' ' . __( 'Uhr', 'go_by_henssler' );
    }

    return $details;
}

/**
 * Manage columns for woo orders.
 * Add subcompany column if user is admin and a tour column
 *
 * @param $columns
 * @return mixed
 */
function gobh_shop_order_columns( $columns ) {

    if ( current_user_can( 'manage_options' ) ) {
        $columns['subcompany'] = __( 'Subcompnay', 'go_by_henssler' );
    }
    $columns['tour'] = __( 'Tour', 'go_by_henssler' );

    return $columns;
}
add_filter( 'manage_edit-shop_order_columns', 'gobh_shop_order_columns' ) ;

/**
 * Add columns to orders in dashboard
 *
 * @param $column
 * @param $post_id
 * @return void
 */
function gobh_manage_shop_order_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {
        case 'subcompany' :
            $subcompany_id = get_post_meta( $post->ID, 'subcompany', true );
            if ( ! empty( $subcompany_id ) ) {
                $subcompany = get_post( $subcompany_id );
                if ( ! empty( $subcompany ) && ! is_wp_error( $subcompany ) ) {
                    $url = esc_url( add_query_arg( array( 'post_type' => $post->post_type, 'subcompany' => $subcompany_id ), 'edit.php' ) );
                    echo '<a href="'.$url.'">' . esc_html( $subcompany->post_title ) . '</a>';
                }
            }
            break;
        case 'tour' :
            $subcompany_id = get_post_meta( $post->ID, 'subcompany', true );
            $tour_id       = get_post_meta( $post->ID, 'tour', true );

            if ( ! empty( $subcompany_id ) ) {
                $tour = gobh_get_tour_details_dashboard( $subcompany_id, $tour_id );
                $url  = esc_url( add_query_arg( array( 'post_type' => $post->post_type, 'subcompany' => $subcompany_id, 'tour' => $tour_id ), 'edit.php' ) );
                if ( ! empty( $tour ) ) {
                    echo '<a href="'.$url.'">' . $tour . '</a>';
                }
            }
            break;
        default :
            break;
    }
}
add_action( 'manage_shop_order_posts_custom_column', 'gobh_manage_shop_order_columns', 10, 2 );

/**
 * Limit subcompany and orders visibility
 *
 * @return void
 */
function gobh_manage_orders_dashboard(){

    if ( is_admin() && isset( $_GET['subcompany'] ) ) {
        add_action( 'pre_get_posts', 'gobh_filter_orders_view', 1 );
    }
}
add_action( 'wp_loaded', 'gobh_manage_orders_dashboard' );

/**
 * Filter orders in dashboard by subcompany and tour
 *
 * @param $query
 * @return void
 */
function gobh_filter_orders_view( $query ) {

    if ( $query->get( 'post_type' ) === 'shop_order' ) {
        $meta_query = [];
        $meta_query[0] = [
            'key'   => 'subcompany',
            'value' => $_GET['subcompany'],
        ];

        if ( isset( $_GET['tour'] ) ) {
            $meta_query[1] = [
                'key'   => 'tour',
                'value' => $_GET['tour'],
            ];
        }

        $query->set( 'meta_query', $meta_query );
        $query->set( 'subcompany', '' );
        $query->set( 'name', '' );
    }
}

/**
 * Filter number of processing orders in woocommerce admin menu
 *
 * @param $count
 * @return int|mixed
 */
function gobh_menu_order_count( $count ) {
    $user            = wp_get_current_user();
    $user_subcompany = get_user_meta( $user->ID, 'assigned_subcompany', true );

    if ( ! empty( $user_subcompany ) && ! in_array( 'administrator', $user->roles ) ) {
        $args = [
            'post_type'   => 'shop_order',
            'numberposts' => -1,
            'post_status' => 'wc_processing',
            'meta_key'    => 'subcompany',
            'meta_value'  => $user_subcompany,
        ];
        $count = count( get_posts( $args ) );
    }

    return $count;
}
add_filter( 'woocommerce_menu_order_count', 'gobh_menu_order_count' );

/**
 * Manipulate number of custom posts if user is restricted to a single subcompany
 *
 * @param $views
 * @return mixed
 */
function gobh_custom_view_count( $views ){
    global $current_screen;

    $user            = wp_get_current_user();
    $user_subcompany = get_user_meta( $user->ID, 'assigned_subcompany', true );

    if ( ! empty( $user_subcompany ) && ! in_array( 'administrator', $user->roles ) ) {
        switch( $current_screen->id ) {
            case 'edit-subcompany':
                $views = gobh_manipulate_subcompany_views( $views );
                break;
            case 'edit-shop_order':
                $views = gobh_manipulate_order_views( $user_subcompany, $views );
                break;
        }
    }

    return $views;
}
add_filter( "views_edit-subcompany" , 'gobh_custom_view_count', 10, 1);
add_filter( "views_edit-shop_order" , 'gobh_custom_view_count', 10, 1);

/**
 * Manipulate subcompanies count if user is restricted to single subcompany only
 *
 * @param $views
 * @return mixed
 */
function gobh_manipulate_subcompany_views( $views ) {
    $views['all']     = preg_replace( '/\(.+\)/U', '(1)', $views['all'] );
    $views['publish'] = preg_replace( '/\(.+\)/U', '(1)', $views['publish'] );

    if ( ! empty( $views['draft'] ) ) {
        $views['draft'] = preg_replace( '/\(.+\)/U', '(0)', $views['draft'] );
    }
    if ( ! empty( $views['pending'] ) ) {
        $views['pending'] = preg_replace( '/\(.+\)/U', '(0)', $views['pending'] );
    }
    if ( ! empty( $views['trash'] ) ) {
        $views['trash'] = preg_replace( '/\(.+\)/U', '(0)', $views['trash'] );
    }

    return $views;
}

/**
 * Manipulate orders count if user is restricted to single subcompany only
 *
 * @param $subcompany_id
 * @param $views
 * @return mixed
 */
function gobh_manipulate_order_views( $subcompany_id, $views ) {

    $views['all'] = preg_replace( '/\(.+\)/U', '(' . gobh_get_orders_count_restricted( $subcompany_id, 'all' ) . ')', $views['all'] );

    if ( ! empty( $views['trash'] ) ) {
        $views['trash'] = preg_replace( '/\(.+\)/U', '(' . gobh_get_orders_count_restricted( $subcompany_id, 'trash' ) . ')', $views['trash'] );
    }

    $order_statuses = wc_get_order_statuses();

    foreach ( $order_statuses as $order_status => $label ) {
        if ( ! empty( $views[$order_status] ) ) {
            $views[$order_status] = preg_replace( '/\(.+\)/U', '(' . gobh_get_orders_count_restricted( $subcompany_id, $order_status ) . ')', $views[$order_status] );
        }
    }

    return $views;
}

/**
 * Get amount of orders based on subcompany meta value and status
 *
 * @param $subcompany_id
 * @param $status
 * @return int
 */
function gobh_get_orders_count_restricted( $subcompany_id, $status ){

    $args = [
        'post_type'   => 'shop_order',
        'numberposts' => -1,
        'meta_key'    => 'subcompany',
        'meta_velue'  => $subcompany_id,
        'post_status' => ( $status === 'all' ) ? array_keys( wc_get_order_statuses() ) : $status,
    ];

    return count( get_posts( $args ) );
}

/**
 * Add buttons for generating driver and kitchen overviews
 *
 * @param $views
 * @return mixed
 */
function gobh_add_options_to_generate_custom_views( $views ){
    if ( isset( $_GET['tour'] ) && isset( $_GET['subcompany'] ) ) {
        $views['gobh-generate-driver-overview']  = '<a target="_blank" href="'.trailingslashit( site_url() ).'wp-admin/edit.php?post_type=subcompany&page=tour-overview&subcompany='.$_GET['subcompany'].'&tour='.$_GET['tour'].'&overview=driver">' . __( 'Driver Overview' ) . '</a>';
        $views['gobh-generate-kitchen-overview'] = '<a target="_blank" href="'.trailingslashit( site_url() ).'wp-admin/edit.php?post_type=subcompany&page=tour-overview&subcompany='.$_GET['subcompany'].'&tour='.$_GET['tour'].'&overview=kitchen">' . __( 'Kitchen Overview' ) . '</a>';
    }

    return $views;
}
add_filter( 'views_edit-shop_order', 'gobh_add_options_to_generate_custom_views', 10, 1 );

/**
 * @return void
 */
function gobh_custom_view_button_styles() {

    $cur_screen = get_current_screen();

    if ( $cur_screen->id === 'edit-shop_order' ) {
        echo '<style>
        .gobh-generate-driver-overview a,
        .gobh-generate-kitchen-overview a {
            width: 100%;
            height: 100%;
            background: #fff;
            border-radius: 4px;
            padding: 10px;
            border: 1px solid #2271B3;
        }
        .gobh-generate-driver-overview a:hover,
        .gobh-generate-kitchen-overview a:hover {
            background: #c1c1c1;
            color: #fff;
            border-color: #000;
        }
        </style>';
    }
}
add_action( 'admin_head', 'gobh_custom_view_button_styles' );

/**
 * Register tour overview page
 *
 * @return void
 */
function gobh_tour_overview_page_register() {
    add_submenu_page(
        'edit.php?post_type=subcompany',
        'Tour Overview',
        'Tour Overview',
        'edit_pages',
        'tour-overview',
        'gobh_tour_overview_content',
        3
    );
}
add_action( 'admin_menu', 'gobh_tour_overview_page_register' );

/**
 * Content for tour overview page
 *
 * @return void
 */
function gobh_tour_overview_content() {
    global $title;

    echo '<div class="wrap">';
    echo '<h1>' . $title . '</h1>';

    if ( isset( $_GET['subcompany'] ) && isset( $_GET['tour'] ) && isset( $_GET['overview'] ) ) {
        echo '<h3>' . gobh_get_tour_details_dashboard( $_GET['subcompany'], $_GET['tour'] ) . '</h3>';
        switch ( $_GET['overview'] ) {
            case 'kitchen':
                echo gobh_render_kitchen_overview( $_GET['subcompany'], $_GET['tour'] );
                break;
            case 'driver':
                echo gobh_render_driver_overview( $_GET['subcompany'], $_GET['tour'] );
                break;
        }
    }

    echo '</div>';
    echo '<style>td,th{padding:10px;min-width:100px;vertical-align:top;}td>p{margin:0;padding:0;}</style>';
}

/**
 * Render content for kitchen tour overview
 *
 * @param $subcompany
 * @param $tour
 * @return string
 */
function gobh_render_kitchen_overview( $subcompany, $tour ) {
    $thead   = '<th>' . __( 'Bestellnummer', 'go_by_henssler' ) . '</th><th>' . __( 'Anzahl', 'go_by_henssler' ) . '</th><th>' . __( 'Produkt', 'go_by_henssler' ) . '</th><th>' . __( 'SKU', 'go_by_henssler' ) . '</th><th>' . __( 'Hinweise', 'go_by_henssler' ) . '</th>';
    $content = '<table><thead><tr>' . $thead . '</tr></thead><tbody>';
    $tbody   = '';
    $tour_orders = gobh_get_tour_orders( $subcompany, $tour );
    $tour_products = [];

    foreach ( $tour_orders as $tour_order ) {
        $order = wc_get_order( $tour_order->ID );

        if ( ! empty( $order ) && ! is_wp_error( $order ) ) {

            $order_items = $order->get_items();

            $tbody .= '<tr>';
            $tbody .= '<td>#' . $order->get_id() . '</td>';
            $tbody .= '<td>';
            foreach ( $order_items as $item ) {
                $tbody .= '<p style="text-align: right;">' . $item->get_quantity() . '</p>';
            }
            $tbody .= '</td>';
            $tbody .= '<td>';
            foreach ( $order_items as $item ) {
                $tbody .= '<p>' . $item->get_name() . '</p>';
            }
            $tbody .= '</td>';
            $tbody .= '<td>';
            foreach ( $order_items as $item ) {
                $product = $item->get_product();
                $sku     = $product->get_sku();
                $tbody .= '<p>' . $sku . '</p>';

                if ( array_key_exists( $sku, $tour_products ) ) {
                    $tour_products[$sku]['qty'] += $item->get_quantity();
                } else {
                    $tour_products[$sku] = [
                        'name' => $item->get_name(),
                        'qty'  => $item->get_quantity(),
                    ];
                }
            }
            $tbody .= '</td>';

            $tbody .= '<td>' . $order->get_customer_note() . '</td>';
            $tbody .= '</tr>';
        }
    }

    $content .= $tbody . '</tbody></table>';

    $totals_table = '';

    if ( ! empty( $tour_products ) ) {
        $totals_table .= '<hr><h3>' . __( 'Total amount of products', 'go_by_henssler' ) . '</h3>';
        $totals_table .= '<table><thead><tr><th>' . __( 'Anzahl', 'go_by_henssler' ) . '</th><th>' . __( 'Produkt', 'go_by_henssler' ) . '</th><th>' . __( 'SKU', 'go_by_henssler' ) . '</th></tr></thead><tbody>';
        foreach ( $tour_products as $sku => $tour_product ) {
            $totals_table .= '<tr>';
            $totals_table .= '<td style="text-align: right;">' . $tour_product['qty'] . '</td>';
            $totals_table .= '<td>' . $tour_product['name'] . '</td>';
            $totals_table .= '<td>' . $sku . '</td>';
            $totals_table .= '</tr>';
        }
        $totals_table .= '</tbody></table>';
        $totals_table .= '<hr><h3>' . __( 'Additional information', 'go_by_henssler' ) . '</h3>';
    }

    return $totals_table . $content;
}

/**
 * Render content for driver tour overview
 *
 * @param $subcompany
 * @param $tour
 * @return string
 */
function gobh_render_driver_overview( $subcompany, $tour ) {
    $thead   = '<th>' . __( 'Bestellnummer', 'go_by_henssler' ) . '</th><th>' . __( 'Kunde', 'go_by_henssler' ) . '</th><th>' . __( 'Tel. Nr', 'go_by_henssler' ) . '</th><th>' . __( 'Anzahl', 'go_by_henssler' ) . '</th><th>' . __( 'Produkt', 'go_by_henssler' ) . '</th><th>' . __( 'SKU', 'go_by_henssler' ) . '</th><th>' . __( 'Hinweise', 'go_by_henssler' ) . '</th>';
    $content = '<table><thead><tr>' . $thead . '</tr></thead><tbody>';
    $tbody   = '';
    $tour_orders  = gobh_get_tour_orders( $subcompany, $tour );

    foreach ( $tour_orders as $tour_order ) {
        $order = wc_get_order( $tour_order->ID );

        if ( ! empty( $order ) && ! is_wp_error( $order ) ) {

            $order_items = $order->get_items();

            $tbody .= '<tr>';
            $tbody .= '<td>#' . $order->get_id() . '</td>';
            $tbody .= '<td>' . $order->get_billing_first_name() . ' ' . $order->get_billing_last_name() . '</td>';
            $tbody .= '<td>' . $order->get_billing_phone() . '</td>';
            $tbody .= '<td>';
            foreach ( $order_items as $item ) {
                $tbody .= '<p style="text-align: right;">' . $item->get_quantity() . '</p>';
            }
            $tbody .= '</td>';
            $tbody .= '<td>';
            foreach ( $order_items as $item ) {
                $tbody .= '<p>' . $item->get_name() . '</p>';
            }
            $tbody .= '</td>';
            $tbody .= '<td>';
            foreach ( $order_items as $item ) {
                $product = $item->get_product();
                $tbody .= '<p>' . $product->get_sku() . '</p>';
            }
            $tbody .= '</td>';

            $tbody .= '<td>' . $order->get_customer_note() . '</td>';
            $tbody .= '</tr>';
        }
    }

    $content .= $tbody . '</tbody></table>';

    return $content;
}

/**
 * Get all orders related to a tour
 *
 * @param $subcompany
 * @param $tour
 * @return int[]|WP_Post[]
 */
function gobh_get_tour_orders( $subcompany, $tour ) {
    $args = [
        'post_type'   => 'shop_order',
        'numberposts' => -1,
        'post_status' => 'wc-processing',
        'meta_query'  => [
            [
                'key'   => 'subcompany',
                'value' => $subcompany,
            ],
            [
                'key'   => 'tour',
                'value' => $tour,
            ],
        ],
    ];

    return get_posts( $args );
}

/**
 * Output this when product has no short description
 *
 * @return string|void
 */
function gobh_product_short_description_fallback(){
    return __( 'Für dieses Produkt existieren keine Angaben zu Zusatzstoffen oder Allergenen', 'go_by_henssler' );
}

/**
 * Filter the recipient email address when woocommerce is trying to send an email notification about new, canceled or failed order
 *
 * @param $recipient
 * @param $order
 * @return mixed
 */
function gobh_filter_new_order_recipient( $recipient, $order ) {

    if ( ! empty( $order ) && ! empty( $order->get_id() ) ) {
        $subcompany_id = get_post_meta( $order->get_id(), 'subcompany', true );

        if ( ! empty( $subcompany_id ) ) {
            $from_email = get_post_meta( $subcompany_id, 'from_email', true );

            if ( ! empty( $from_email ) ) {
                $recipient = $from_email;
            }
        }
    }

    return $recipient;
}
add_filter( 'woocommerce_email_recipient_new_order', 'gobh_filter_new_order_recipient', 99, 2 );
add_filter( 'woocommerce_email_recipient_cancelled_order', 'gobh_filter_new_order_recipient', 99, 2 );
add_filter( 'woocommerce_email_recipient_failed_order', 'gobh_filter_new_order_recipient', 99, 2 );
