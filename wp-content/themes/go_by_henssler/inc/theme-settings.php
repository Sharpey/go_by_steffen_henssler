<?php
/* ----------------------------------------------------------------------------
 * Add Theme settings page for custom options
 * ------------------------------------------------------------------------- */

class Gobh_Settings_Page {
    /**
     * Array of custom settings/options
     **/
    private $options;

    /**
     * Constructor
     */
    public function __construct() {
        add_action( 'admin_menu', array( $this, 'add_settings_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add settings page
     * The page will appear in Admin menu
     */
    public function add_settings_page() {
        add_menu_page(
            __( 'Theme Settings', 'go_by_henssler' ),
            __( 'Theme Settings', 'go_by_henssler'),
            'edit_pages',
            'gobh-custom-settings',
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page() {
        // Set class property
        $this->options = get_option( 'gobh_settings' );
        ?>
        <div class="wrap">
            <h2><?php echo get_admin_page_title(); ?></h2>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields( 'gobh_settings_group' );
                do_settings_sections( 'gobh-custom-settings' );
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init() {

        register_setting(
            'gobh_settings_group',
            'gobh_settings', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'gobh_settings_section', // ID
            __( 'Theme Settings', 'go_by_henssler' ), // Title
            array( $this, 'gobh_settings_section' ), // Callback
            'gobh-custom-settings' // Page
        );

        add_settings_field(
            'gobh_instagram_link', // ID
            __( 'Instagram Link', 'go_by_henssler' ), // Title
            array( $this, 'gobh_instagram_link_field' ), // Callback
            'gobh-custom-settings', // Page
            'gobh_settings_section'
        );

        add_settings_field(
            'gobh_facebook_link',
            __( 'Facebook Link', 'go_by_henssler' ),
            array( $this, 'gobh_facebook_link_field' ),
            'gobh-custom-settings',
            'gobh_settings_section'
        );
    }

    /**
     * Sanitize POST data from custom settings form
     *
     * @param array $input Contains custom settings which are passed when saving the form
     */
    public function sanitize( $input ) {

        $sanitized_input = [];

        if ( isset( $input['gobh_instagram_link'] ) ) {
            $sanitized_input['gobh_instagram_link'] = sanitize_text_field( $input['gobh_instagram_link'] );
        }

        if ( isset( $input['gobh_facebook_link'] ) ) {
            $sanitized_input['gobh_facebook_link'] = sanitize_text_field( $input['gobh_facebook_link'] );
        }

        return $sanitized_input;
    }

    /**
     * Custom settings callback
     */
    public function gobh_settings_section() {}

    /**
     * HTML for Instagram Link setting input
     */
    public function gobh_instagram_link_field() {
        printf(
            '<input type="text" id="gobh_instagram_link" name="gobh_settings[gobh_instagram_link]" value="%s" />',
            isset( $this->options['gobh_instagram_link'] ) ? esc_attr( $this->options['gobh_instagram_link'] ) : ''
        );
    }

    /**
     * HTML for Facebook Link setting input
     */
    public function gobh_facebook_link_field() {
        printf(
            '<input type="text" id="gobh_facebook_link" name="gobh_settings[gobh_facebook_link]" value="%s" />',
            isset( $this->options['gobh_facebook_link'] ) ? esc_attr( $this->options['gobh_facebook_link'] ) : ''
        );
    }
}

new Gobh_Settings_Page();
