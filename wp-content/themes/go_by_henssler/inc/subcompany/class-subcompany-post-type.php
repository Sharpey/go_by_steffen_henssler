<?php

namespace GOBH\SubCompany;

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

class SubCompany_Post_Type {

    public function __construct() {
        add_action( 'init', [ $this, 'register_post_type' ], 5 );
    }

    public static function register_post_type() {
        if ( post_type_exists( 'subcompany' ) ) {
            return;
        }

        $labels = array(
            'name'                => _x( 'SubCompanies', 'Post Type General Name', 'go_by_henssler' ),
            'singular_name'       => _x( 'SubCompany', 'Post Type Singular Name', 'go_by_henssler' ),
            'menu_name'           => __( 'SubCompanies', 'go_by_henssler' ),
            'all_items'           => __( 'SubCompanies', 'go_by_henssler' ),
            'view_item'           => __( 'View SubCompany', 'go_by_henssler' ),
            'add_new_item'        => __( 'Add New SubCompany', 'go_by_henssler' ),
            'add_new'             => __( 'New SubCompany', 'go_by_henssler' ),
            'edit_item'           => __( 'Edit SubCompany', 'go_by_henssler' ),
            'update_item'         => __( 'Update SubCompany', 'go_by_henssler' ),
            'search_items'        => __( 'Search SubCompanies', 'go_by_henssler' ),
            'not_found'           => __( 'No SubCompanies found', 'go_by_henssler' ),
            'not_found_in_trash'  => __( 'No SubCompanies found in Trash', 'go_by_henssler' ),
        );

        $settings = array(
            'labels'              => $labels,
            'description'         => __( 'SubCompany information pages', 'go_by_henssler' ),
            'supports'            => array( 'title' ),
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-store',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => true,
            'publicly_queryable'  => true,
            'query_var'           => true,
            'capability_type'     => 'page',
            'map_meta_cap'        => true,
            'hierarchical'        => true
        );

        register_post_type( 'subcompany', $settings );

        $tax_labels = array(
            'name'              => _x( 'Categories', 'taxonomy general name', 'go_by_henssler' ),
            'singular_name'     => _x( 'Category', 'taxonomy singular name', 'go_by_henssler' ),
            'search_items'      => __( 'Search Category', 'go_by_henssler' ),
            'all_items'         => __( 'All Categories', 'go_by_henssler' ),
            'parent_item'       => __( 'Parent Category', 'go_by_henssler' ),
            'parent_item_colon' => __( 'Parent Category:', 'go_by_henssler' ),
            'edit_item'         => __( 'Edit Category', 'go_by_henssler' ),
            'update_item'       => __( 'Update Category', 'go_by_henssler' ),
            'add_new_item'      => __( 'Add New Category', 'go_by_henssler' ),
            'new_item_name'     => __( 'New Category', 'go_by_henssler' ),
            'menu_name'         => __( 'Categories', 'go_by_henssler' ),
        );

        $tax_args = array(
            'hierarchical'      => true,
            'labels'            => $tax_labels,
            'show_admin_column' => true,
            'query_var'         => true,
            'show_in_nav_menus' => true,
        );

        register_taxonomy( 'subcompany_category', array( 'subcompany' ), $tax_args );

    }
    
}

new SubCompany_Post_Type();
