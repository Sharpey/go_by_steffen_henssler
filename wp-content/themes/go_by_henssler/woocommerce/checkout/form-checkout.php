<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! empty( $_REQUEST['client-email'] ) ) {
    $data = [
        'billing_email' => $_REQUEST['client-email'],
    ];

    $persons_number = isset( $_REQUEST['persons_number'] ) ? intval( $_REQUEST['persons_number'] ) : 0;
    $data['order_comments'] = __('Anzahl der Personen') . ': ' . $persons_number . ';';

    if ( ! empty( $_REQUEST['note'] ) ) {
        $data['order_comments'] = $data['order_comments'] . ' ' . $_REQUEST['note'];
    }

    if ( ! empty( $_REQUEST['client-phone'] ) ) {
        $data['billing_phone'] = $_REQUEST['client-phone'];
    }

    if ( ! empty( $_REQUEST['client-name'] ) ) {
        $client_names = explode( ' ', $_REQUEST['client-name'] );
        $data['billing_first_name'] = array_shift( $client_names );
        $data['billing_last_name']  = array_pop( $client_names );
    }

    $data['payment_method'] = 'ppcp-gateway';

    $order_id = $checkout->create_order( $data );

    if ( ! empty( $order_id ) && ! is_wp_error( $order_id ) ) {
        $order = wc_get_order( $order_id );
        $order->add_meta_data( 'subcompany', $_SESSION['subcompany'], true );
        $order->add_meta_data( 'tour', $_SESSION['tour'], true );
        $order->add_meta_data( 'anzahl', $persons_number, true );
        $order->save_meta_data();
        $pay_url = $order->get_checkout_payment_url();

        if ( ! empty( $pay_url ) ) {
            wp_redirect( $pay_url );
        }
    }
}

$cart_totals = gobh_get_cart_content();
$subtotal    = ( ! empty( $cart_totals['subtotal'] ) ) ? $cart_totals['subtotal'] : '0 &euro;';
$total_sum   = ( ! empty( $cart_totals['total_sum'] ) ) ? $cart_totals['total_sum'] : '0 &euro;';

?>

<div class="order-box">
    <div class="container-1142">
        <form name="checkout" method="post" class="order-box-form" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data" novalidate>
            <span class="order-box-title">
                <?php _e( 'Warenkorb', 'go_by_henssler' ); ?>
            </span>
            <span class="order-box-subtitle">
                <?php _e( 'Produktauswahl', 'go_by_henssler' ); ?>
            </span>
            <ul class="order-box-list">
                <?php echo gobh_get_cart_details( true ); ?>
            </ul>
            <div class="order-box-row">
                <div class="order-box-point">
                    <div class="order-box-info order-box-note">
                        <label for="note">
                            <?php _e( 'Notiz für das Restaurant', 'go_by_henssler' ); ?>
                        </label>
                        <textarea name="note" id="note"></textarea>
                    </div>
                </div>
            </div>
            <div class="order-box-row">
                <div class="order-box-point">
                    <span class="order-box-subtitle">
                        <?php _e( 'Abholtermin', 'go_by_henssler' ); ?>
                    </span>
                    <div class="order-box-info order-box-info-date">
                        <label for="date">
                            <?php _e( 'Datum', 'go_by_henssler' ); ?>
                        </label>
                        <span id="date"><?php echo esc_html( $_SESSION['tour_date'] ); ?></span>
                        <label for="time">
                            <?php _e( 'Uhrzeit', 'go_by_henssler' ); ?>
                        </label>
                        <span id="time"><?php echo substr( esc_html( $_SESSION['tour_start_hr'] ), 0, -3 ); ?>-<?php echo substr( esc_html( $_SESSION['tour_end_hr'] ), 0, -3 ); ?> <?php _e( 'Uhr', 'go_by_henssler' ); ?></span>
                    </div>
                </div>
                <div class="order-box-point">
                    <span class="order-box-subtitle">
                        <?php _e( 'Abholadresse', 'go_by_henssler' ); ?>
                    </span>
                    <div class="order-box-info order-box-info-text info-address">
                        <span><?php echo esc_html( $_SESSION['tour_address'] ); ?></span>
                    </div>
                </div>
            </div>
            <div class="order-box-row">
                <div class="order-box-point">
                        <span class="order-box-subtitle">
                            <?php _e( 'Anzahl der Personen', 'go_by_henssler' ); ?>
                        </span>
                    <div class="order-box-info">
                        <div class="qty-small-box">
                            <div class="qty-small-info">
                                <span class="qty-small-label">
                                    <?php _e( 'Anzahl', 'go_by_henssler' ); ?>
                                </span>
                                <span id="persons_number_front" class="qty-small-number">1</span>
                                <input id="persons_number" type="hidden" name="persons_number" value="1" />
                            </div>
                            <div class="qty-small-count qty-count">
                                <span class="qty-count-less icon-circle-minus persons-less"></span>
                                <span class="qty-count-more icon-circle-plus persons-more"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="order-box-row">
                <span class="order-box-subtitle">
                    <?php _e( 'Persönliche Daten', 'go_by_henssler' ); ?>
                </span>
                <div class="order-box-point personal-data">
                    <div class="order-box-info order-box-info-text">
                        <label for="client-name">
                            <?php _e( 'Name', 'go_by_henssler' ); ?>
                        </label>
                        <input type="text" name="client-name" class="required-input" id="client-name" value="" />
                    </div>
                    <span class="order-box-info-error">
                        <?php _e( 'Das Namensfeld darf nicht leer sein', 'go_by_henssler' ); ?>
                    </span>
                </div>
                <div class="order-box-point personal-data">
                    <div class="order-box-info order-box-info-text">
                        <label for="client-name">
                            <?php _e( 'Email', 'go_by_henssler' ); ?>
                        </label>
                        <input type="email" name="client-email" class="required-input" id="client-email" value="" />
                    </div>
                    <span class="order-box-info-error">
                        <?php _e( 'Die E-Mailadresse muss das Symbol @ und einen Punkt enthalten', 'go_by_henssler' ); ?>
                    </span>
                </div>
                <div class="order-box-point personal-data">
                    <div class="order-box-info order-box-info-text">
                        <label for="client-name">
                            <?php _e( 'Mobile', 'go_by_henssler' ); ?>
                        </label>
                        <div class="tel-block">
                            <input type="number" name="client-phone" class="required-input" id="client-phone" value="" />
                        </div>
                    </div>
                    <span class="order-box-info-error">
                        <?php _e( 'Das Telefonfeld darf nicht leer sein', 'go_by_henssler' ); ?>
                    </span>
                </div>
            </div>
            <div class="order-box-row">
                <div class="order-box-point">
                        <span class="order-box-subtitle">
                            <?php _e( 'Bezahlmethode', 'go_by_henssler' ); ?>
                        </span>
                    <div class="order-box-info order-box-info-text">
                        <span>PayPal</span>
                    </div>
                </div>
            </div>
            <div class="order-box-sum">
                    <span class="order-box-subtitle">
                        <?php _e( 'Summe', 'go_by_henssler' ); ?>
                    </span>
                <div class="order-box-subtotal">
                    <span class="title">
                        <?php _e( 'Zwischensumme', 'go_by_henssler' ); ?>
                    </span>
                    <span id="cart-details-subtotal" class="price"><?php echo esc_html( $subtotal ); ?></span>
                </div>
                <div class="order-box-total">
                    <span class="title">
                        <?php _e( 'Gesamt', 'go_by_henssler' ); ?>
                    </span>
                    <span id="cart-details-total" class="price"><?php echo esc_html( $total_sum ); ?></span>
                </div>
            </div>
            <button class="btn order-box-button">
                    <span>
                        <?php _e( 'Jetzt zahlungspflichtig bestellen', 'go_by_henssler' ); ?>
                    </span>
            </button>
        </form>
    </div>
</div>
