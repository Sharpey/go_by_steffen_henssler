<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 */

defined( 'ABSPATH' ) || exit;

get_header();
$prod_cats = gobh_get_product_categories();
?>

    <main id="primary" class="site-main">
        <div class="container-1376">

            <?php
                if ( ! empty( $prod_cats ) ) {
                    get_template_part( 'template-parts/content', 'categories-list', $prod_cats );
                }
            ?>

            <div class="content-shop">
                <?php
                if ( empty( $prod_cats ) ) {
                    do_action( 'woocommerce_no_products_found' );
                } else {
                    foreach ( $prod_cats as $cat ) {
                        get_template_part( 'template-parts/content', 'shop-category', ['cat' => $cat] );
                    }
                }
                ?>
            </div>

        </div>
    </main><!-- #main -->

    <?php get_template_part( 'template-parts/content', 'box-details' ); ?>

<?php
get_footer();
