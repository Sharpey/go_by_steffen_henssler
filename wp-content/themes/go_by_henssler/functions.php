<?php
if ( ! session_id() ) {
    ini_set('session.gc_maxlifetime', 1800);
    session_set_cookie_params(1800);
    session_start();
}
/**
 * go_by_henssler functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package go_by_henssler
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'go_by_henssler_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function go_by_henssler_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on go_by_henssler, use a find and replace
		 * to change 'go_by_henssler' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'go_by_henssler', get_template_directory() . '/languages' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'footer-menu' => esc_html__( 'Footer Menu', 'go_by_henssler' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'go_by_henssler_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);

        // Support woocommerce
        add_theme_support( 'woocommerce' );
	}
endif;
add_action( 'after_setup_theme', 'go_by_henssler_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function go_by_henssler_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'go_by_henssler_content_width', 640 );
}
add_action( 'after_setup_theme', 'go_by_henssler_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function go_by_henssler_scripts() {
	wp_enqueue_style( 'go_by_henssler-style', get_template_directory_uri() . '/dist/css/style.css', [], _S_VERSION );
	wp_enqueue_script( 'main-js', trailingslashit( get_stylesheet_directory_uri() ) . '/dist/js/main.min.js', [], false, true );
	wp_style_add_data( 'go_by_henssler-style', 'rtl', 'replace' );
}
add_action( 'wp_enqueue_scripts', 'go_by_henssler_scripts' );

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/* Use classic widgets */
add_filter( 'use_widgets_block_editor', '__return_false' );

/**
 * Register SubCompany post type
 */
require get_template_directory() . '/inc/subcompany/class-subcompany-post-type.php';

/**
 * ACF custom fields
 */
include_once get_template_directory() . '/inc/acf/subcompany-fields.php';
include_once get_template_directory() . '/inc/acf/user-fields.php';

/**
 * Theme Settings
 */
include_once get_template_directory() . '/inc/theme-settings.php';

/**
 * Hide admin pages from shop manager
 *
 * @return void
 */
function gobh_remove_menu_items(){

    $roles = wp_get_current_user()->roles;

    if ( in_array('shop_manager', $roles ) ) {
        remove_menu_page( 'index.php' );                  //Dashboard
        remove_menu_page( 'jetpack' );                    //Jetpack*
        remove_menu_page( 'upload.php' );                 //Media
        remove_menu_page( 'edit.php?post_type=page' );    //Pages
        remove_menu_page( 'edit.php?post_type=product' ); //Products
        remove_menu_page( 'edit-comments.php' );          //Comments
        remove_menu_page( 'themes.php' );                 //Appearance
        remove_menu_page( 'plugins.php' );                //Plugins
        remove_menu_page( 'users.php' );                  //Users
        remove_menu_page( 'profile.php' );                //Users
        remove_menu_page( 'tools.php' );                  //Tools
        remove_menu_page( 'options-general.php' );        //Settings
        remove_menu_page( 'edit.php' );                   //Posts
        remove_menu_page( 'wpseo_workouts' );             //Yoast Settings
        remove_menu_page( 'gobh-custom-settings' );       //Custom theme settings

        remove_submenu_page( 'edit.php?post_type=subcompany', 'post-new.php?post_type=subcompany' ); // New Subcompany
        remove_submenu_page( 'edit.php?post_type=shop_order', 'post-new.php?post_type=shop_order' ); // New Order
    }

}
add_action( 'admin_menu', 'gobh_remove_menu_items', 9999 );

/**
 * Hide more menu items from shop manager
 *
 * @return void
 */
function gobh_remove_other_menu_items(){

    $roles = wp_get_current_user()->roles;

    if ( in_array('shop_manager', $roles ) ) {
        echo '<style>
            #wpbody-content > .wrap > .page-title-action,
            #menu-posts-subcompany > .wp-submenu-wrap > li:last-of-type {
                display: none;
            }
        </style>';
    }

}
add_action( 'admin_head', 'gobh_remove_other_menu_items' );
