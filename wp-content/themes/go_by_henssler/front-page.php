<?php
/**
 * The Template for homepage (map, places and tours)
 */

get_header('empty');

$gm_api_key = 'AIzaSyADHUCyj9Pl-nChm3iOdrOeKjcgKVdrZJQ';
$map_id     = '9fb56277cb0b8c1a';
$locations  = gobh_get_map_locations();
?>


    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/handlebars/4.7.7/handlebars.min.js"></script>
    <script>
        'use strict';

        /** Hide a DOM element. */
        function hideElement(el) {
            el.style.display = 'none';
        }

        /** Show a DOM element that has been hidden. */
        function showElement(el) {
            el.style.display = 'block';
        }

        /**
         * Defines an instance of the Locator+ solution, to be instantiated
         * when the Maps library is loaded.
         */
        let markersGlobal = false;

        function LocatorPlus(configuration) {
            const locator = this;

            locator.locations = configuration.locations || [];
            locator.capabilities = configuration.capabilities || {};

            const mapEl = document.getElementById('map');
            const panelEl = document.getElementById('locations-panel');
            locator.panelListEl = document.getElementById('locations-panel-list');
            const sectionNameEl =
                document.getElementById('location-results-section-name');
            const resultsContainerEl = document.getElementById('location-results-list');

            const itemsTemplate = Handlebars.compile(
                document.getElementById('locator-result-items-tmpl').innerHTML);

            locator.searchLocation = null;
            locator.searchLocationMarker = null;
            locator.selectedLocationIdx = null;
            locator.userCountry = null;

            // Initialize the map -------------------------------------------------------
            locator.map = new google.maps.Map(mapEl, configuration.mapOptions);

            // Store selection.
            const selectResultItem = function(locationIdx, panToMarker, scrollToResult) {
                locator.selectedLocationIdx = locationIdx;
                for (let locationElem of resultsContainerEl.children) {
                    locationElem.classList.remove('selected');
                    if (getResultIndex(locationElem) === locator.selectedLocationIdx) {
                        locationElem.classList.add('selected');
                        if (scrollToResult) {
                            panelEl.scrollTop = locationElem.offsetTop;
                        }
                    }
                }
                if (panToMarker && (locationIdx != null)) {
                    locator.map.panTo(locator.locations[locationIdx].coords);
                }
            };

            // Create a marker for each location.
            const markers = locator.locations.map(function(location, index) {
                const icons = {
                    black: 'wp-content/themes/go_by_henssler/dist/img/marker.png',
                    red: 'wp-content/themes/go_by_henssler/dist/img/marker-red.png',
                };
                const marker = new google.maps.Marker({
                    position: location.coords,
                    map: locator.map,
                    title: location.title,
                    icon: icons[location.color],
                    subCompanies: (location.color === 'red') ? true : false,
                    url: location.url,
                });
                marker.addListener('click', function() {
                    if(marker.subCompanies) {
                        window.location.href = marker.url;
                    }
                });
                return marker;
            });

            markersGlobal = markers;

            // Fit map to marker bounds.
            locator.updateBounds = function() {
                const bounds = new google.maps.LatLngBounds();
                if (locator.searchLocationMarker) {
                    bounds.extend(locator.searchLocationMarker.getPosition());
                }
                for (let i = 0; i < markers.length; i++) {
                    bounds.extend(markers[i].getPosition());
                }
                locator.map.fitBounds(bounds);
            };
            if (locator.locations.length) {
                locator.updateBounds();
            }

            // Get the distance of a store location to the user's location,
            // used in sorting the list.
            const getLocationDistance = function(location) {
                if (!locator.searchLocation) return null;

                // Use travel distance if available (from Distance Matrix).
                if (location.travelDistanceValue != null) {
                    return location.travelDistanceValue;
                }

                // Fall back to straight-line distance.
                return google.maps.geometry.spherical.computeDistanceBetween(
                    new google.maps.LatLng(location.coords),
                    locator.searchLocation.location);
            };

            // Render the results list --------------------------------------------------
            const getResultIndex = function(elem) {
                return parseInt(elem.getAttribute('data-location-index'));
            };

            locator.renderResultsList = function() {
                let locations = locator.locations.slice();
                for (let i = 0; i < locations.length; i++) {
                    locations[i].index = i;
                }
                if (locator.searchLocation) {
                    sectionNameEl.textContent =
                        'Nearest locations (' + locations.length + ')';
                    locations.sort(function(a, b) {
                        return getLocationDistance(a) - getLocationDistance(b);
                    });
                } else {
                    sectionNameEl.textContent = `All locations (${locations.length})`;
                }
                locations.sort(function (a, b) {
                    const pattern = /(\d{2})\.(\d{2})\.(\d{4})/;
                    a.date = new Date(((a.address1).substr(5)).replace(pattern,'$3-$2-$1'));
                    b.date = new Date(((b.address1).substr(5)).replace(pattern,'$3-$2-$1'));
                    if (Date.parse(a.date)) {
                        a.date = Date.parse(a.date);
                    } else {
                        a.date = 0;
                    }
                    if (Date.parse(b.date)) {
                        b.date = Date.parse(b.date);
                    } else {
                        b.date = 0;
                    }
                    return a.date - b.date;
                });
                const resultItemContext = {
                    locations: locations,
                    showDirectionsButton: !!locator.searchLocation
                };
                resultsContainerEl.innerHTML = itemsTemplate(resultItemContext);
                for (let item of resultsContainerEl.children) {
                    const resultsContainerHint = document.querySelector('.location-results-hint');
                    resultsContainerHint.style.display = 'block';

                    const resultIndex = getResultIndex(item);
                    if (resultIndex === locator.selectedLocationIdx) {
                        item.classList.add('selected');
                    }

                    const resultSelectionHandler = function() {
                        if (resultIndex !== locator.selectedLocationIdx) {
                            locator.clearDirections();
                        }
                        selectResultItem(resultIndex, true, false);
                    };

                    // Clicking anywhere on the item selects this location.
                    // Additionally, create a button element to make this behavior
                    // accessible under tab navigation.
                    item.addEventListener('click', resultSelectionHandler);
                    item.querySelector('.select-location')
                        .addEventListener('click', function(e) {
                            resultSelectionHandler();
                            e.stopPropagation();
                        });
                }

                for ( let i = 0; i < markers.length; i++) {
                    markers[i].setVisible(true);
                }
                locator.locations.forEach((element) => {
                    if (element.travelDistanceStatus === false) {
                        for ( let i = 0; i < markers.length; i++) {
                            if ( markers[i].title === element.title ) {
                                markers[i].setVisible(false);
                            }
                        }
                    }
                })
            };

            /** Calculate distance between locations. */
            locator.updateTravelTimes = function() {
                if (!locator.searchLocation) return;

                locator.locations.forEach(function(location) {
                    location.travelDistanceStatus = false;
                    let slLat = locator.searchLocation.location.lat();
                    let slLng = locator.searchLocation.location.lng();

                    if ( typeof location.coords.lat !== 'undefined'
                        && typeof location.coords.lng !== 'undefined'
                        && typeof slLat !== 'undefined'
                        && typeof slLng !== 'undefined'
                    ) {
                        let travelDistanceValue = distanceBetweenCoordinates(location.coords.lat, location.coords.lng, slLat, slLng);
                        location.travelDistanceValue = travelDistanceValue;
                        if (travelDistanceValue <= 200 && location.color !== 'black') {
                            location.travelDistanceStatus = true;
                        }
                    }
                });

                locator.renderResultsList();
            };

            // Optional capability initialization --------------------------------------
            initializeSearchInput(locator);
            locator.updateTravelTimes();
            initializeDirections(locator);
            initializeDetails(locator);

            // Initial render of results -----------------------------------------------
            locator.renderResultsList();
        }

        /**
         * Convert degrees to radians
         */
        function degreesToRadians(degrees) {
            return degrees * Math.PI / 180;
        }

        /**
         * Calculate distance between two coordinates
         */
        function distanceBetweenCoordinates(lat1, lon1, lat2, lon2) {
            let earthRadiusKm = 6371;

            let dLat = degreesToRadians(lat2-lat1);
            let dLon = degreesToRadians(lon2-lon1);

            lat1 = degreesToRadians(lat1);
            lat2 = degreesToRadians(lat2);

            let a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
            let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

            return earthRadiusKm * c;
        }

        /** When the search input capability is enabled, initialize it. */
        function initializeSearchInput(locator) {
            const geocodeCache = new Map();
            const geocoder = new google.maps.Geocoder();

            const searchInputEl = document.getElementById('location-search-input');
            const searchButtonEl = document.getElementById('location-search-button');
            const searchUserLocation = document.querySelector('.search-input-icon');

            searchUserLocation.addEventListener("click", () => {
                // Try HTML5 geolocation.
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(
                        (position) => {
                            const pos = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude,
                            };
                            geocoder.geocode({ location: pos }).then((response) => {
                                geocodeSearch(response.results[0].formatted_address);
                            })
                        },
                    );
                }
            });

            const updateSearchLocation = function(address, location) {
                showAllMakers();
                if (locator.searchLocationMarker) {
                    locator.searchLocationMarker.setMap(null);
                }
                if (!location) {
                    locator.searchLocation = null;
                    return;
                }
                locator.searchLocation = {'address': address, 'location': location};
                locator.searchLocationMarker = new google.maps.Marker({
                    position: location,
                    map: locator.map,
                    title: 'Mein Standort',
                    icon: 'wp-content/themes/go_by_henssler/dist/img/marker-white.png',
                });

                // Update the locator's idea of the user's country, used for units. Use
                // `formatted_address` instead of the more structured `address_components`
                // to avoid an additional billed call.
                const addressParts = address.split(' ');
                locator.userCountry = addressParts[addressParts.length - 1];

                // Update map bounds to include the new location marker.
                locator.updateBounds();

                // Update the result list so we can sort it by proximity.
                locator.renderResultsList();

                locator.updateTravelTimes();

                locator.clearDirections();
            };

            const geocodeSearch = function(query) {
                if (!query) {
                    return;
                }

                const handleResult = function(geocodeResult) {
                    searchInputEl.value = geocodeResult.formatted_address;
                    updateSearchLocation(
                        geocodeResult.formatted_address, geocodeResult.geometry.location);
                };

                if (geocodeCache.has(query)) {
                    handleResult(geocodeCache.get(query));
                    return;
                }
                const request = {address: query, bounds: locator.map.getBounds()};
                geocoder.geocode(request, function(results, status) {
                    if (status === 'OK') {
                        if (results.length > 0) {
                            const result = results[0];
                            geocodeCache.set(query, result);
                            handleResult(result);
                        }
                    }
                });
            };
            // Remove disabled class from markers
            const showAllMakers = () => {
                for ( let i = 0; i < markersGlobal.length; i++) {
                    markersGlobal[i].setVisible(true);
                }
            };

            // Checking the input for changes
            searchInputEl.oninput = () => {
                showAllMakers();
            };

            // Set up geocoding on the search input.
            searchButtonEl.addEventListener('click', function() {
                showAllMakers();
                geocodeSearch(searchInputEl.value.trim());
            });

            // Initialize Autocomplete.
            initializeSearchInputAutocomplete(locator, searchInputEl, geocodeSearch, updateSearchLocation);
        }

        /** Add Autocomplete to the search input. */
        function initializeSearchInputAutocomplete(
            locator, searchInputEl, fallbackSearch, searchLocationUpdater) {
            // Set up Autocomplete on the search input. Bias results to map viewport.
            const autocomplete = new google.maps.places.Autocomplete(searchInputEl, {
                types: ['geocode'],
                fields: ['place_id', 'formatted_address', 'geometry.location']
            });
            autocomplete.bindTo('bounds', locator.map);
            autocomplete.addListener('place_changed', function() {
                const placeResult = autocomplete.getPlace();
                if (!placeResult.geometry) {
                    // Hitting 'Enter' without selecting a suggestion will result in a
                    // placeResult with only the text input value as the 'name' field.
                    fallbackSearch(placeResult.name);
                    return;
                }
                searchLocationUpdater(
                    placeResult.formatted_address, placeResult.geometry.location);
            });
        }

        /** Initialize Directions service for the locator. */
        function initializeDirections(locator) {
            const directionsCache = new Map();
            const directionsService = new google.maps.DirectionsService();
            const directionsRenderer = new google.maps.DirectionsRenderer({
                suppressMarkers: true,
            });

            // Update directions displayed from the search location to
            // the selected location on the map.
            locator.updateDirections = function() {
                if (!locator.searchLocation || (locator.selectedLocationIdx == null)) {
                    return;
                }
                const cacheKey = JSON.stringify(
                    [locator.searchLocation.location, locator.selectedLocationIdx]);
                if (directionsCache.has(cacheKey)) {
                    const directions = directionsCache.get(cacheKey);
                    directionsRenderer.setMap(locator.map);
                    directionsRenderer.setDirections(directions);
                    return;
                }
                const request = {
                    origin: locator.searchLocation.location,
                    destination: locator.locations[locator.selectedLocationIdx].coords,
                    travelMode: google.maps.TravelMode.DRIVING
                };
                directionsService.route(request, function(response, status) {
                    if (status === 'OK') {
                        directionsRenderer.setMap(locator.map);
                        directionsRenderer.setDirections(response);
                        directionsCache.set(cacheKey, response);
                    }
                });
            };

            locator.clearDirections = function() {
                directionsRenderer.setMap(null);
            };
        }

        /** Initialize Place Details service and UI for the locator. */
        function initializeDetails(locator) {
            const panelDetailsEl = document.getElementById('locations-panel-details');
            const detailsService = new google.maps.places.PlacesService(locator.map);

            const detailsTemplate = Handlebars.compile(
                document.getElementById('locator-details-tmpl').innerHTML);

            const renderDetails = function(context) {
                panelDetailsEl.innerHTML = detailsTemplate(context);
                panelDetailsEl.querySelector('.back-button')
                    .addEventListener('click', hideDetails);
            };

            const hideDetails = function() {
                showElement(locator.panelListEl);
                hideElement(panelDetailsEl);
            };

            locator.showDetails = function(locationIndex) {
                const location = locator.locations[locationIndex];
                const context = {location};

                // Helper function to create a fixed-size array.
                const initArray = function(arraySize) {
                    const array = [];
                    while (array.length < arraySize) {
                        array.push(0);
                    }
                    return array;
                };

                if (location.placeId) {
                    const request = {
                        placeId: location.placeId,
                        fields: [
                            'formatted_phone_number', 'website', 'opening_hours', 'url',
                            'utc_offset_minutes', 'price_level', 'rating', 'user_ratings_total'
                        ]
                    };
                    detailsService.getDetails(request, function(place, status) {
                        if (status == google.maps.places.PlacesServiceStatus.OK) {
                            if (place.opening_hours) {
                                const daysHours =
                                    place.opening_hours.weekday_text.map(e => e.split(/\:\s+/))
                                        .map(e => ({'days': e[0].substr(0, 3), 'hours': e[1]}));

                                for (let i = 1; i < daysHours.length; i++) {
                                    if (daysHours[i - 1].hours === daysHours[i].hours) {
                                        if (daysHours[i - 1].days.indexOf('-') !== -1) {
                                            daysHours[i - 1].days =
                                                daysHours[i - 1].days.replace(/\w+$/, daysHours[i].days);
                                        } else {
                                            daysHours[i - 1].days += ' - ' + daysHours[i].days;
                                        }
                                        daysHours.splice(i--, 1);
                                    }
                                }
                                place.openingHoursSummary = daysHours;
                            }
                            if (place.rating) {
                                const starsOutOfTen = Math.round(2 * place.rating);
                                const fullStars = Math.floor(starsOutOfTen / 2);
                                const halfStars = fullStars !== starsOutOfTen / 2 ? 1 : 0;
                                const emptyStars = 5 - fullStars - halfStars;

                                // Express stars as arrays to make iterating in Handlebars easy.
                                place.fullStarIcons = initArray(fullStars);
                                place.halfStarIcons = initArray(halfStars);
                                place.emptyStarIcons = initArray(emptyStars);
                            }
                            if (place.price_level) {
                                place.dollarSigns = initArray(place.price_level);
                            }
                            if (place.website) {
                                const url = new URL(place.website);
                                place.websiteDomain = url.hostname;
                            }

                            context.place = place;
                            renderDetails(context);
                        }
                    });
                }
                renderDetails(context);
                hideElement(locator.panelListEl);
                showElement(panelDetailsEl);
            };
        }
    </script>
    <script id="map_locations">
        const CONFIGURATION = {
            "locations": [
            <?php foreach ( $locations as $location ): ?>
                {
                    "title":"<?php echo esc_attr( $location->title ); ?>",
                    "address1":"<?php echo esc_attr( $location->address ); ?>",
                    "date":"",
                    "coords":{"lat":<?php echo esc_attr( $location->lat ); ?>,"lng":<?php echo esc_attr( $location->lon ); ?>},
                    "placeId":"<?php echo esc_attr( $location->place_id ); ?>",
                    "color": "<?php echo esc_attr( $location->color ); ?>",
                    "url": "<?php echo esc_attr( $location->url ); ?>"
                },
                <?php foreach ( $location->location_tours as $tour ): ?>
                {
                    "title":"<?php echo esc_attr( $tour->title ); ?>",
                    "address1":"<?php echo gobh_convert_tour_date( $tour->date ); ?>",
                    "date":"",
                    "coords":{"lat":<?php echo esc_attr( $tour->lat ); ?>,"lng":<?php echo esc_attr( $tour->lon ); ?>},
                    "placeId":"<?php echo esc_attr( $tour->place_id ); ?>",
                    "color": "<?php echo esc_attr( $tour->color ); ?>",
                    "url": "<?php echo esc_attr( $tour->url ); ?>",
                },
                <?php endforeach ?>
            <?php endforeach ?>
            ],
            "mapOptions": {"mapId": "<?php echo esc_attr( $map_id ); ?>", "center":{"lat":59.09,"lng":10.02},"fullscreenControl": false,"mapTypeControl":false,"streetViewControl":false,"zoom":6,"zoomControl":false,"maxZoom":10, "minZoom":6, "backgroundColor":'#000000'},
            "mapsApiKey": "<?php echo esc_attr( $gm_api_key ); ?>"
        };

        function initMap() {
            new LocatorPlus(CONFIGURATION);
        }
    </script>
    <script id="locator-result-items-tmpl" type="text/x-handlebars-template">
        {{#each locations}}
        {{#if travelDistanceStatus}}
        <li class="location-result" data-location-index="{{index}}">
            <a class="location-result-link" href="{{url}}">
                <button class="select-location">
                    <h2 class="name">{{title}}</h2>
                </button>
                <div class="address">{{address1}},{{address2}}</div>
            </a>
        </li>
        {{/if}}
        {{/each}}
    </script>
    <script id="locator-details-tmpl" type="text/x-handlebars-template">
        <button class="back-button">
            <img class="icon" src="https://fonts.gstatic.com/s/i/googlematerialicons/arrow_back/v11/24px.svg" alt=""/>
            Back
        </button>
        <header>
            <div class="banner">
                <svg width="23" height="32" viewBox="0 0 23 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M22.9976 11.5003C22.9976 13.2137 22.7083 14.9123 21.8025 16.7056C18.6321 22.9832 12.7449 24.3314 12.2758 30.7085C12.2448 31.1294 11.9286 31.4744 11.4973 31.4744C11.0689 31.4744 10.7527 31.1294 10.7218 30.7085C10.2527 24.3314 4.3655 22.9832 1.19504 16.7056C0.289306 14.9123 0 13.2137 0 11.5003C0 5.13275 5.14557 0 11.5003 0C17.852 0 22.9976 5.13275 22.9976 11.5003Z" fill="#4285F4"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" transform="translate(5.5,5.5)" d="M6 8.84091L9.708 11L8.724 6.92961L12 4.19158L7.6856 3.83881L6 0L4.3144 3.83881L0 4.19158L3.276 6.92961L2.292 11L6 8.84091Z" fill="#FBE15C"/>
                </svg>
            </div>
            <h2>{{location.title}}</h2>
        </header>
        <div class="address">
            {{location.address1}}<br>
            {{location.address2}}
        </div>
        <div class="atmosphere">
            {{#if place.rating}}
            <span class="star-rating-numeric">{{place.rating}}</span>
            <span>
            {{#each place.fullStarIcons}}
              <img src="https://fonts.gstatic.com/s/i/googlematerialicons/star/v15/24px.svg"
                   alt="" class="star-icon"/>
            {{/each}}
            {{#each place.halfStarIcons}}
              <img src="https://fonts.gstatic.com/s/i/googlematerialicons/star_half/v17/24px.svg"
                   alt="" class="star-icon"/>
            {{/each}}
            {{#each place.emptyStarIcons}}
              <img src="https://fonts.gstatic.com/s/i/googlematerialicons/star_outline/v9/24px.svg"
                   alt="" class="star-icon"/>
            {{/each}}
          </span>
            {{/if}}
            {{#if place.user_ratings_total}}
            <a href="{{place.url}}" target="_blank">{{place.user_ratings_total}} reviews</a>
            {{else}}
            <a href="{{place.url}}" target="_blank">See on Google Maps</a>
            {{/if}}
            {{#if place.price_level}}
            &bull;
            <span class="price-dollars">
            {{#each place.dollarSigns}}${{/each}}
          </span>
            {{/if}}
        </div>
        <hr/>
        {{#if place.opening_hours}}
        <div class="hours contact">
            <img src="https://fonts.gstatic.com/s/i/googlematerialicons/schedule/v12/24px.svg"
                 alt="Opening hours" class="icon"/>
            <div class="right">
                {{#each place.openingHoursSummary}}
                <div>
                    <span class="weekday">{{days}}</span>
                    <span class="hours">{{hours}}</span>
                </div>
                {{/each}}
            </div>
        </div>
        {{/if}}
        {{#if place.website}}
        <div class="website contact">
            <img src="https://fonts.gstatic.com/s/i/googlematerialicons/public/v10/24px.svg"
                 alt="Website" class="icon"/>
            <div class="right">
                <a href="{{place.website}}" target="_blank">{{place.websiteDomain}}</a>
            </div>
        </div>
        {{/if}}
        {{#if place.formatted_phone_number}}
        <div class="phone contact">
            <img src="https://fonts.gstatic.com/s/i/googlematerialicons/phone/v10/24px.svg"
                 alt="Phone number" class="icon"/>
            <div class="right">
                {{place.formatted_phone_number}}
            </div>
        </div>
        {{/if}}
        {{#if place.html_attributions}}
        {{#each place.html_attributions}}
        <p class="attribution">{{{this}}}</p>
        {{/each}}
        {{/if}}
    </script>
    <div class="map-page">
        <div id="map-container" class="map-page-container">
            <div id="map" class="map"></div>
            <div id="locations-panel" class="locations-panel">
                <div class="locations-panel-logo">
                    <svg fill="#fffef2" preserveAspectRatio="xMidYMid meet" viewBox="0 0 286 81">
                        <path d="M99,31.2c1.7-0.9,3.5-1.3,5.4-1.3c1.5,0,2.9,0.3,4.3,0.8c1.4,0.6,2.6,1.4,3.6,2.4l0.4,0.4l4.3-4.2l-0.3-0.4c-1.5-1.7-3.3-3-5.4-3.8c-2.3-0.9-4.7-1.3-7.1-1.3c-3,0-6,0.7-8.6,2.1c-2.5,1.3-4.6,3.3-6,5.7c-1.5,2.5-2.2,5.3-2.2,8.2c0,2.9,0.7,5.7,2.2,8.2c1.4,2.4,3.5,4.4,6,5.7c2.6,1.4,5.6,2.1,8.5,2.1c2.2,0,4.4-0.3,6.5-1c2-0.6,3.9-1.6,5.6-2.9l0.2-0.2V39.1H110v9.6c-1.7,0.9-3.7,1.4-5.6,1.3c-1.9,0-3.7-0.4-5.4-1.3c-1.5-0.8-2.8-2.1-3.7-3.6c-0.9-1.6-1.3-3.3-1.3-5.1c0-1.8,0.4-3.6,1.3-5.2C96.2,33.2,97.4,32,99,31.2z M153.9,31.7c-1.4-2.4-3.5-4.4-6-5.7c-2.6-1.4-5.6-2.1-8.5-2.1c-3,0-5.9,0.7-8.5,2.1c-2.5,1.3-4.6,3.3-6,5.7c-1.5,2.5-2.2,5.3-2.2,8.2c0,2.9,0.7,5.7,2.2,8.2c1.4,2.4,3.5,4.4,6,5.7c2.6,1.4,5.6,2.1,8.5,2.1c3,0,5.9-0.7,8.5-2.1c2.5-1.3,4.6-3.3,6-5.7c1.5-2.5,2.2-5.3,2.2-8.2C156.1,37.1,155.4,34.2,153.9,31.7z M144.5,48.7c-1.6,0.8-3.4,1.3-5.1,1.3s-3.6-0.4-5.1-1.3c-1.5-0.8-2.7-2.1-3.6-3.6c-0.9-1.6-1.3-3.4-1.3-5.2c0-1.8,0.4-3.6,1.3-5.2c0.8-1.5,2.1-2.7,3.6-3.6c1.6-0.8,3.4-1.3,5.1-1.3c1.8,0,3.6,0.4,5.1,1.3c1.5,0.8,2.7,2.1,3.6,3.6c0.9,1.6,1.3,3.4,1.3,5.2c0,1.8-0.4,3.6-1.3,5.2C147.2,46.6,146,47.9,144.5,48.7zM168.9,35.9c0.1-0.1,0.1-0.1,0.2-0.2c0-0.1,0.1-0.2,0.1-0.3c0-0.4-0.3-0.6-0.9-0.6h-1.4v1.3h1.4C168.5,36.1,168.7,36,168.9,35.9L168.9,35.9z M166.8,32.7v1.2h1.2c0.2,0,0.5,0,0.7-0.2c0.1,0,0.1-0.1,0.2-0.2c0-0.1,0.1-0.2,0.1-0.3c0-0.1,0-0.2-0.1-0.3c0-0.1-0.1-0.1-0.2-0.2c-0.2-0.1-0.4-0.2-0.7-0.1H166.8zM170.1,34.7c0.2,0.2,0.3,0.5,0.2,0.8c0,0.2,0,0.4-0.1,0.6c-0.1,0.2-0.2,0.3-0.4,0.4c-0.5,0.3-1,0.4-1.5,0.4h-2.6v-5.1h2.5c0.5,0,1,0.1,1.4,0.4c0.2,0.1,0.3,0.3,0.4,0.4c0.1,0.2,0.1,0.4,0.1,0.6c0,0.2-0.1,0.5-0.2,0.7c-0.1,0.2-0.3,0.4-0.5,0.5C169.7,34.4,169.9,34.5,170.1,34.7M174.1,35.1V37h-1.2v-1.8l-2-3.3h1.3l1.4,2.3l1.4-2.3h1.2L174.1,35.1z M181.5,36.8c-0.7-0.2-1.4-0.5-2-0.9l0.9-1.9c0.5,0.4,1.1,0.7,1.7,0.8c0.6,0.2,1.3,0.3,2,0.3c0.6,0,1.1-0.1,1.6-0.3c0.2-0.1,0.3-0.2,0.4-0.4c0.1-0.2,0.1-0.3,0.1-0.5c0-0.1,0-0.3-0.1-0.4c-0.1-0.1-0.1-0.2-0.2-0.3c-0.2-0.2-0.5-0.3-0.8-0.4c-0.3-0.1-0.8-0.2-1.3-0.4c-0.7-0.2-1.4-0.4-2.1-0.6c-0.5-0.2-1-0.5-1.4-1c-0.4-0.5-0.6-1.1-0.6-1.8c0-0.6,0.2-1.2,0.5-1.7c0.4-0.6,0.9-1,1.6-1.2c0.8-0.3,1.7-0.5,2.6-0.5c0.7,0,1.4,0.1,2.1,0.3c0.6,0.1,1.2,0.4,1.8,0.7l-0.8,1.9c-0.9-0.6-2-0.9-3.1-0.9c-0.6,0-1.1,0.1-1.6,0.4c-0.2,0.1-0.3,0.2-0.4,0.4c-0.1,0.2-0.1,0.3-0.1,0.5c0,0.2,0.1,0.4,0.2,0.5s0.3,0.3,0.4,0.3c0.6,0.2,1.2,0.4,1.8,0.5c0.7,0.2,1.4,0.4,2.1,0.6c0.5,0.2,1,0.5,1.4,0.9c0.4,0.5,0.6,1.1,0.6,1.8c0,0.6-0.2,1.2-0.5,1.7c-0.4,0.6-1,1-1.6,1.2c-0.8,0.3-1.7,0.5-2.6,0.5C183.2,37.2,182.4,37.1,181.5,36.8 M193.8,28h-3.5v-2.1h9.6V28h-3.5v9h-2.6V28z M211,34.9V37h-8.6V25.9h8.4v2.1H205v2.4h5.1v2H205v2.6H211z M216.8,27.9v2.9h5.1v2.1h-5.1v4h-2.6V25.9h8.4v2.1H216.8z M228.1,27.9v2.9h5.1v2.1h-5.1v4h-2.6V25.9h8.4v2.1H228.1z M245.3,34.9V37h-8.6V25.9h8.4v2.1h-5.8v2.4h5.1v2h-5.1v2.6H245.3z M258.7,25.9V37h-2.1l-5.5-6.7V37h-2.5V25.9h2.1l5.5,6.7v-6.7H258.7z M173.3,42.9v4.4h-5v-4.4h-2.6V54h2.6v-4.5h5V54h2.6V42.9H173.3z M179.6,42.9V54h8.6v-2.1h-6v-2.6h5.1v-2h-5.1V45h5.8v-2.1H179.6z M199,42.9v6.7l-5.5-6.7h-2.1V54h2.5v-6.7l5.5,6.7h2.1V42.9H199z M206.4,53.8c-0.7-0.2-1.4-0.5-2-0.9l0.9-1.9c0.5,0.4,1.1,0.6,1.7,0.8c0.6,0.2,1.3,0.3,2,0.3c0.6,0,1.1-0.1,1.6-0.3c0.2-0.1,0.3-0.2,0.4-0.4c0.1-0.2,0.1-0.3,0.1-0.5c0-0.1,0-0.3-0.1-0.4c-0.1-0.1-0.1-0.2-0.2-0.3c-0.2-0.2-0.5-0.3-0.8-0.4c-0.3-0.1-0.8-0.2-1.3-0.4c-0.7-0.2-1.4-0.4-2.1-0.6c-0.5-0.2-1-0.5-1.4-1c-0.4-0.5-0.6-1.1-0.6-1.8c0-0.6,0.2-1.2,0.5-1.8c0.4-0.6,0.9-1,1.6-1.2c0.8-0.3,1.7-0.5,2.6-0.5c0.7,0,1.4,0.1,2.1,0.3c0.6,0.1,1.2,0.4,1.8,0.7l-0.8,1.9c-0.9-0.6-2-0.9-3.1-0.9c-0.6,0-1.1,0.1-1.6,0.4c-0.2,0.1-0.3,0.2-0.4,0.4c-0.1,0.2-0.1,0.3-0.1,0.5c0,0.2,0.1,0.4,0.2,0.5c0.1,0.2,0.3,0.3,0.4,0.3c0.6,0.2,1.2,0.4,1.8,0.5c0.7,0.2,1.4,0.4,2.1,0.6c0.5,0.2,1,0.5,1.4,1c0.4,0.5,0.6,1.1,0.6,1.8c0,0.6-0.2,1.2-0.5,1.7c-0.4,0.6-1,1-1.6,1.2c-0.8,0.3-1.7,0.5-2.6,0.5C208.1,54.2,207.3,54.1,206.4,53.8 M217.7,53.8c-0.7-0.2-1.4-0.5-2-0.9l0.9-1.9c0.5,0.4,1.1,0.6,1.7,0.8c0.6,0.2,1.3,0.3,2,0.3c0.6,0,1.1-0.1,1.6-0.3c0.2-0.1,0.3-0.2,0.4-0.4c0.1-0.2,0.1-0.3,0.1-0.5c0-0.1,0-0.3-0.1-0.4c-0.1-0.1-0.1-0.2-0.2-0.3c-0.2-0.2-0.5-0.3-0.8-0.4c-0.3-0.1-0.8-0.2-1.3-0.4c-0.7-0.2-1.4-0.4-2.1-0.6c-0.5-0.2-1-0.5-1.4-0.9c-0.4-0.5-0.6-1.1-0.6-1.8c0-0.6,0.2-1.2,0.5-1.8c0.4-0.6,0.9-1,1.6-1.2c0.8-0.3,1.7-0.5,2.6-0.5c0.7,0,1.4,0.1,2.1,0.3c0.6,0.1,1.2,0.4,1.8,0.7l-0.8,1.9c-0.9-0.6-2-0.9-3.1-0.9c-0.6,0-1.1,0.1-1.6,0.4c-0.2,0.1-0.3,0.2-0.4,0.4s-0.1,0.3-0.1,0.5c0,0.2,0.1,0.4,0.2,0.5c0.1,0.2,0.3,0.3,0.4,0.3c0.6,0.2,1.2,0.4,1.8,0.5c0.7,0.2,1.4,0.4,2.1,0.6c0.5,0.2,1,0.5,1.4,1c0.4,0.5,0.6,1.1,0.6,1.8c0,0.6-0.2,1.2-0.5,1.7c-0.4,0.6-0.9,1-1.6,1.2c-0.8,0.3-1.7,0.5-2.6,0.5C219.4,54.2,218.5,54.1,217.7,53.8 M227.8,42.9V54h8.1v-2.1h-5.6v-9H227.8z M238.6,42.9V54h8.6v-2.1h-6v-2.6h5.1v-2h-5.1V45h5.8v-2.1H238.6z M256.7,45.5c-0.5-0.4-1.1-0.5-1.8-0.5h-2.1v3.9h2.1c0.6,0,1.3-0.1,1.8-0.5c0.2-0.2,0.3-0.4,0.4-0.7c0.1-0.2,0.2-0.5,0.2-0.8c0-0.3-0.1-0.5-0.2-0.8C257.1,45.9,256.9,45.7,256.7,45.5L256.7,45.5z M257.4,54l-2.1-3.1h-2.4V54h-2.6V42.9h4.8c0.9,0,1.7,0.1,2.6,0.5c0.5,0.2,0.9,0.6,1.3,1c0.4,0.4,0.6,0.9,0.8,1.5c0.2,0.5,0.2,1.1,0.1,1.6c-0.1,0.6-0.3,1.1-0.6,1.6c-0.4,0.6-1,1.1-1.7,1.4l2.5,3.6L257.4,54z M61.8,0h-60v9.3h60V0z M39.6,45c-0.5-0.7-1.4-1.7-3.2-1.5l-0.7,1c3.2-0.7,3.1,1.7,4.8,2.5c0.2-0.3,0.3-0.6,0.5-0.8C40.6,46.1,40.1,45.8,39.6,45z M36.5,42.9c0.1-0.2,0.1-1.4,0-1.4c-0.2,0.1-0.3,0.1-0.5,0.2c-0.2,0.1-0.1,1.2,0,1.3C36.1,43.1,36.4,43.1,36.5,42.9z M45.6,40c0,0,0.7,0.4,0.7,0.9c0.1,0.4-0.6,1.2-0.6,1.2c0.4-0.2,0.8-0.4,1.3-0.5c-0.1-1.1-0.1-2.2,0.2-3.2C45.8,38.6,45.6,40,45.6,40z M31,32.2c-0.2,0.5-0.4,1-0.5,1.6c0.2,0.2,0.4,0.6,0.6,0.9c0.2-0.2,0.4-0.2,0.7-0.2h0.1H32c0.2,0,0.5,0.1,0.7,0.2c0.1-0.3,0.3-0.7,0.6-0.9c-0.1-0.5-0.2-1.1-0.5-1.6c-0.1,0.1-0.2,0.2-0.4,0.2c-0.2,0.1-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5-0.1C31.3,32.3,31.1,32.3,31,32.2z M29.4,34.9c-0.2,0-0.3,0-0.4-0.1c0.4,0.7,0.8,1.4,0.9,2.3c0.1-0.7,0.2-1.4,0.7-2c-0.1-0.2-0.2-0.4-0.2-0.6c-0.1,0.1-0.2,0.2-0.4,0.2C29.8,34.8,29.6,34.9,29.4,34.9zM31.8,36c-0.2,0-0.3,0-0.5-0.1c-0.2-0.1-0.2-0.2-0.4-0.2c-0.2,0.6-0.5,1.2-0.6,1.8c0.6,0,1.2,0.2,1.4,0.5c0.2-0.3,0.8-0.5,1.4-0.5c-0.1-0.7-0.2-1.2-0.6-1.8c-0.1,0.1-0.2,0.2-0.4,0.2C32.2,35.9,32,36,31.8,36z M34.7,34.8c-0.2,0.1-0.3,0.1-0.4,0.1c-0.2,0-0.3,0-0.5-0.1c-0.2-0.1-0.2-0.2-0.4-0.2c-0.1,0.2-0.2,0.4-0.2,0.6c0.3,0.6,0.6,1.3,0.7,2C33.9,36.2,34.2,35.5,34.7,34.8zM47.3,42.9c-0.3,0.4-0.5,1-0.6,1.5c0.3,0,0.6-0.1,0.9-0.2c0-0.1-0.1-0.2-0.1-0.3C47.4,43.4,47.3,43.1,47.3,42.9z M42.1,40.9c0.3,0.4,0.5,0.9,0.5,1.4c0,1.2-1.1,2.2-2.5,2.4l0,0c-0.7,0.1,0.7,1,1.7,0.7c0.5-0.1,1-0.3,1.3-0.7c0.3-0.4,0.6-0.9,0.6-1.4c0.1-1.1-0.5-1.8-1.1-2.2C42.1,40.5,41.9,40.6,42.1,40.9L42.1,40.9z M34.3,33.2c0.2,0,0.4,0.1,0.6,0.2c0.2,0.1,0.3,0.2,0.5,0.3c0.2-0.4,0.5-0.9,0.7-1.3c0.1-0.2,0.2-0.5,0.3-0.8c-0.2,0.1-0.4,0.2-0.7,0.2c-0.2,0-0.3,0-0.5-0.1c-0.2-0.1-0.2-0.2-0.4-0.2C34.6,32,34.4,32.6,34.3,33.2z M37.7,28.9c-0.1,0-0.2,0-0.3,0c-0.2,0-0.3,0-0.5-0.1s-0.2-0.2-0.4-0.2c-0.2,0.5-0.4,1.1-0.5,1.7c0.3,0.1,0.6,0.2,0.7,0.4C37.1,30,37.4,29.4,37.7,28.9z M33.7,33.3c0.1-1.1,0.4-2.3,1.2-2.9c-0.1-0.4-0.2-0.8-0.4-1.2c-0.2,0.2-0.5,0.3-0.8,0.3c-0.3,0-0.7-0.2-0.8-0.3c-0.2,0.6-0.4,1.2-0.5,1.7C33.1,31.2,33.5,32.3,33.7,33.3z M41.3,34.2c0,0-0.2,0.9-0.9,1c0,0.6-0.2,1.2-0.5,1.7c0.6-0.2,1.2-0.6,1.7-1C41.7,35.2,41.6,34.7,41.3,34.2z M34.4,39c-0.4,0.7-0.7,1.4-1.4,1.5c0.2,0.2,0.3,0.4,0.5,0.5c0.2,0.1,0.5,0.2,0.7,0.2c0.9,0,1.6-0.7,1.6-0.7c0,0.2-0.1,0.4-0.2,0.6s-0.3,0.2-0.5,0.3c0,0,0.7,0.1,1.2-0.9c0.6-1.1,1.5-1.1,1.5-1.1s-0.7,0.4-0.8,0.8c-0.1,0.2-0.2,0.3-0.2,0.5c0,0,0.6,0,1.7-0.7c1.2-0.8,3.2-0.2,3.2-0.2C41,39,40,39,40,39s0.2-0.3,1.2-0.3c0.9,0,1.2-0.2,1.6-0.9c-0.1,0.1-0.6,0.3-1.9,0.3c-1.2,0-2.2,1-2.2,1s0-0.6,0.7-1.1c0.6-0.3,1-0.6,1.2-1.1c-0.7,0.4-1.3,0.7-2.2,0.7c0.3-0.3,0.6-0.7,0.7-1s0.2-0.7,0.3-1c-0.4,0.7-0.9,1.2-1.5,1.6c-0.7,0.4-1.2,1-1.4,1.7c0-0.4,0.1-0.8,0.2-1.2c0.2-0.3,0.4-0.7,0.7-1c-0.3,0.1-0.6,0.2-0.8,0.4c-0.2,0.2-0.4,0.5-0.6,0.7c-0.3,0.7-1.1,2.2-2.2,2.3c0.3-0.2,0.6-0.7,0.7-1.1c0.1-0.4,0.3-0.7,0.7-1c-0.2,0-0.5,0.1-0.7,0.2C34.7,38.6,34.6,38.8,34.4,39zM41,40.5c-0.2-0.2-0.4,0-0.2,0.2c0.2,0.2,0.3,0.4,0.4,0.7c0.1,0.2,0.2,0.5,0.2,0.7c0,0.3-0.1,0.7-0.2,0.9c-0.2,0.3-0.4,0.5-0.7,0.7c-0.3,0.2,0.2,0.3,0.5,0.2c0.3-0.2,0.6-0.4,0.8-0.7c0.2-0.3,0.3-0.7,0.3-1s-0.1-0.7-0.2-1C41.7,41.1,41.4,40.7,41,40.5z M42,34.3c0.2-0.9,0.7-1.7,1.7-1.8h0.1c0.2,0,0.3,0,0.6,0.1c0.2,0.1,0.3,0.2,0.4,0.3c0.1,0,0.1-0.1,0.2-0.1c0.2-0.8,0.2-1.7,0.1-2.6c-0.2,0.7-0.6,1.2-1.2,1.7c-0.7,0.6-1.6,1-2.5,1.2C41.5,33.6,41.8,33.9,42,34.3z M28.6,41.9c-0.4,0-0.2,0.4-0.2,0.7c0,0.3,0,0.6,0.2,0.6c0.2,0,1.1-1.3,2.7-1.6c-0.6,0.4-1.1,0.8-1.6,1.3C29,43.7,29,43.9,29,44c0.1,0.2,0.5,0.6,0.7,0.6c0.2,0,0.7-0.7,1.7-0.5c-0.4,0.2-0.7,0.5-1,0.9c-0.4,0.7-0.4,0.7-0.2,1.3c0.2,0.6,0.3,3.4,1.7,3.4c1.2,0,1.5-2.8,1.7-3.4s0.2-0.6-0.2-1.3c-0.2-0.4-0.6-0.7-1-0.9c1-0.2,1.4,0.5,1.7,0.5c0.2,0,0.7-0.4,0.7-0.6c0.1-0.2,0-0.4-0.7-1.1c-0.5-0.5-1-1-1.6-1.3c1.6,0.2,2.5,1.6,2.7,1.6c0.2,0,0.2-0.2,0.2-0.6c0-0.3,0.2-0.7-0.2-0.7c-0.9,0.1-1.9-0.2-2.7-1.6c0.2,0.1,0.4,0,0.7-0.1c0.2-0.1,0.4-0.2,0.5-0.4c0.2-0.5,0.5-1.1,0.9-1.5C34,38.1,33.5,38.1,33,38c-0.7,0-1.1,0.6-1.2,1.6c-0.1-1-0.4-1.6-1.2-1.6c-0.5,0-1,0.1-1.4,0.3c0.4,0.4,0.7,1,0.9,1.5c0.1,0.2,0.3,0.3,0.5,0.4c0.2,0.1,0.4,0.1,0.7,0.1C30.5,41.6,29.5,42,28.6,41.9zM42.6,49.5c-0.1,0-0.2,0.1-0.4,0.2c0.6,0.8,0.9,1.7,1.1,2.7c0.2,0.4,0.3,0.8,0.6,1.2C43.7,50.7,43,49.7,42.6,49.5z M37.4,45.2c-0.9,0-1.3,0.5-1.3,0.5c1.1,0.1,2.1,0.8,2.6,1.8c0.7-0.1,1.3,0.3,1.6,0.5c0.1-0.2,0.1-0.3,0.2-0.5C39.1,46.8,38.2,45.2,37.4,45.2zM38.9,48.1c0.1,0.2,0.1,0.4,0.1,0.7c0.3-0.1,0.7-0.1,1-0.1c0-0.1,0.1-0.2,0.1-0.4C39.6,48.2,39.3,48.1,38.9,48.1z M27.6,45.6c0,0-0.4-0.5-1.3-0.5c-0.9,0-1.7,1.7-3.1,2.3c0.1,0.2,0.2,0.3,0.2,0.5c0.2-0.2,0.9-0.5,1.6-0.5C25.6,46.4,26.6,45.7,27.6,45.6zM46.3,50.7c0.9-2.2-0.5-5-2.6-5.1c-1.9-0.1-2.4,1.7-3.1,3.2c0.3,0.1,0.6,0.2,0.8,0.4c1.3-2.2,2.2-3,3.3-1.8c1.2,1.2,0.5,3.1-0.7,4.2c0.1,0.2,0.1,0.7,0.2,1.3C44.9,52.3,45.8,52.1,46.3,50.7z M37.1,46.7c-1.2-0.7-2.8-0.2-3.1,0.9s-0.9,2.5-2.1,2.5s-1.8-1.3-2.1-2.5s-1.8-1.7-3.1-0.9c-1.2,0.7-1.7,2.6-1.1,3.6c0.6,1,1.7,1.4,2.6,1c0.8-0.4,0.6-1.6,0.6-1.6s-0.6,0.9-1.6,0.6c-0.9-0.3-0.9-1.3-0.6-1.8s1.3-0.9,1.9-0.4c0.6,0.5,1.3,2.2,1.5,2.7c0.2,0.5,0.7,1,1.9,1s1.7-0.5,1.9-1s0.9-2.2,1.5-2.7c0.6-0.5,1.6-0.1,1.9,0.4c0.3,0.5,0.3,1.5-0.6,1.8c-0.9,0.3-1.6-0.6-1.6-0.6s-0.2,1.2,0.6,1.6c0.8,0.4,2,0,2.6-1C38.7,49.2,38.3,47.3,37.1,46.7z M44.3,42.2c0.2,0.4,1.1-0.7,1.1-1c0-0.3-0.7-0.7-0.7-0.7c0.2-4.1,2.6-4.2,2.6-4.2c-0.7,0.6-1.3,1.4-1.7,2.2c0.2-0.2,0.7-0.7,1.7-0.7c0.1-0.4,0.2-0.8,0.4-1.2s0.3-0.7,0.5-1.1c-3.5,0-4.4,4.7-4.6,5.7C43.9,41.5,44.1,41.8,44.3,42.2L44.3,42.2z M46,45.1c-0.1-0.9,0.2-1.7,0.6-2.5c-2,0.9-2.5,2.2-2.5,2.2s1.5,0.2,2.7,2.5c0.5,1,0.7,2.1,0.4,3.2c-0.2,1.1-0.8,2-1.7,2.7c-0.8,0.6-1.2,1.3-0.6,1.7c0.2,0.1,0.3,0.2,0.4,0.2c1.8,0.5,3.2-0.7,3.6-3c-0.3,0-0.6,0.2-0.8,0.4c-0.9,1.1-1.8,0.9-1.8,0.9c0.7-0.3,1.1-0.7,1.3-1.6c0.2-0.7,0.7-0.9,1.5-1c0-0.4,0-0.8-0.1-1.3c-0.3,0-0.6,0.1-0.9,0.2c0.1-0.2,0.2-0.3,0.3-0.4s0.3-0.2,0.5-0.2c-0.1-0.6-0.2-1.2-0.4-1.8c-0.4,0.2-0.7,0.4-1.1,0.7c0.1-0.6,0.4-1.1,0.8-1.4c-0.2-0.6-0.3-1.1-0.5-1.7C47.1,45.2,46,45.1,46,45.1z M44.8,48.4c0-0.2-0.2-0.3-0.2-0.5c0,0.1,0,0.1,0,0.2c0,0.2-0.1,0.4-0.1,0.6c0-0.2,0-0.4-0.1-0.6c0-0.1-0.1-0.2-0.1-0.2s-0.1-0.1-0.1-0.2c-0.1-0.1-0.2-0.1-0.4-0.1c-0.1,0-0.2,0-0.2,0.1c0,0.1,0.1,0.1,0.1,0.2c0.1,0.1,0.1,0.2,0.2,0.3c0.1,0.1,0.1,0.2,0.1,0.3c0,0.1,0.1,0.2,0.1,0.3c-0.1-0.1-0.1-0.2-0.2-0.3c-0.1-0.1-0.1-0.2-0.2-0.3c-0.1-0.1-0.2-0.2-0.2-0.2l-0.1-0.1l0,0C43,48,43,48.3,43.2,48.4c0.8,0.6,0.5,1.7,0.7,1.8C44.3,50.3,44.9,49.2,44.8,48.4z M19.9,45.7c-2,0.1-3.5,2.9-2.6,5.1c0.6,1.3,1.4,1.6,2,2.3c0.1-0.6,0.2-1.1,0.2-1.3c-1.2-1.2-1.9-3.1-0.7-4.2c1.2-1.2,2-0.2,3.3,1.8c0.2-0.2,0.5-0.3,0.8-0.4C22.3,47.4,21.9,45.6,19.9,45.7z M16.4,42.9c-0.1,0.3-0.2,0.7-0.2,1l-0.1,0.3c0.2,0.2,0.6,0.2,0.9,0.2C16.9,43.8,16.7,43.3,16.4,42.9z M27.9,44.6l-0.7-1c-1.8-0.3-2.7,0.7-3.2,1.5c-0.5,0.7-1,1.1-1.5,1.2c0.2,0.2,0.3,0.6,0.5,0.8C24.9,46.2,24.9,43.9,27.9,44.6z M18.1,40c0,0-0.2-1.4-1.7-1.7c0.2,1.1,0.3,2.2,0.2,3.2c0.5,0.1,0.9,0.2,1.3,0.5c0,0-0.6-0.7-0.6-1.2C17.4,40.5,18.1,40,18.1,40z M27.7,43c0.1-0.1,0.2-1.3,0-1.3c-0.2-0.1-0.3-0.1-0.5-0.2c-0.1,0-0.1,1.2,0,1.4C27.3,43.1,27.6,43.1,27.7,43z M21.1,49.5c-0.3,0.2-1.1,1.1-1.3,4.2c0.2-0.4,0.4-0.8,0.6-1.2c0.2-1,0.5-1.9,1.1-2.7C21.2,49.6,21.1,49.5,21.1,49.5z M20.5,48.4c0.2-0.2,0.2-0.4-0.1-0.6l0,0c-0.1,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.2,0.2-0.2,0.2s-0.2,0.2-0.2,0.3c-0.1,0.1-0.1,0.2-0.2,0.3c0-0.1,0-0.2,0.1-0.3c0-0.1,0.1-0.2,0.1-0.3c0.1-0.1,0.1-0.2,0.2-0.3c0-0.1,0.1-0.1,0.1-0.2c-0.1,0-0.2,0-0.2-0.1c-0.2,0-0.2,0-0.4,0.1c0,0.1-0.1,0.1-0.1,0.2s-0.1,0.2-0.1,0.2c-0.1,0.2-0.1,0.4-0.1,0.6c-0.1-0.2-0.1-0.4-0.1-0.6c0-0.1,0-0.1,0-0.2c-0.2,0.2-0.2,0.3-0.2,0.5c-0.1,0.7,0.5,1.9,0.7,1.7C20,50.2,19.6,49,20.5,48.4z M23.6,48.3c0.1,0.2,0.1,0.3,0.1,0.4c0.3,0,0.7,0,1,0.1c0-0.2,0-0.4,0.1-0.7C24.5,48.1,24,48.2,23.6,48.3z M18.2,53.1c-0.8-0.7-1.4-1.7-1.7-2.7c-0.2-1.1-0.1-2.2,0.4-3.2c1.1-2.2,2.7-2.5,2.7-2.5s-0.5-1.3-2.5-2.2c0.4,0.7,0.7,1.7,0.6,2.5c0,0-1.1,0.1-1.7-0.3c-0.2,0.6-0.3,1.1-0.5,1.7c0.4,0.3,0.7,0.9,0.8,1.4c-0.3-0.2-0.7-0.5-1.1-0.7c-0.2,0.6-0.3,1.2-0.4,1.8c0.2,0.1,0.3,0.2,0.5,0.2c0.2,0.1,0.2,0.2,0.3,0.4c-0.3-0.1-0.6-0.2-0.9-0.2c-0.1,0.4-0.1,0.8-0.1,1.3c0.7,0.1,1.2,0.3,1.5,1c0.2,0.8,0.6,1.2,1.3,1.6c0,0-0.9,0.2-1.8-0.9c-0.2-0.2-0.5-0.3-0.8-0.4c0.4,2.3,1.7,3.5,3.6,3c0.2-0.1,0.3-0.1,0.4-0.2C19.3,54.4,18.9,53.6,18.2,53.1zM12.9,55.7L12.9,55.7c0.1,0.4,0.2,0.8,0.2,1.2c0.2,0,0.4,0.1,0.6,0.2c0.2,0.1,0.3,0.2,0.4,0.4c0.5,0.6,2.2,0.3,2.2,0.3C14.3,57.4,13.3,56.3,12.9,55.7z M31.6,55.4c0-0.1-0.1-0.2-0.1-0.2c-0.1-0.1-0.1-0.1-0.2-0.2v0.1c0,0.1,0,0.2-0.1,0.2c-0.1,0.2-0.1,0.3-0.2,0.4c0-0.2,0-0.3,0-0.5c0-0.1,0-0.2,0-0.2c0-0.1,0-0.2-0.1-0.2c-0.1,0-0.2,0-0.3,0c0,0.1,0,0.1,0,0.2c0,0.1,0,0.2,0,0.2c0,0.2,0,0.4-0.1,0.6c0-0.2-0.1-0.3-0.2-0.6c0-0.1-0.1-0.2-0.1-0.2s0-0.1-0.1-0.2c-0.1,0-0.2,0.1-0.2,0.2c-0.2,0.4,0,0.6,0.2,1.2c0.1,0.3,0.2,0.7,0.3,0.7C30.9,57.2,31.7,55.9,31.6,55.4z M29.3,64.1L29.3,64.1c0.2,0.1,0.2,0,0.3-0.1c0.2-0.3-0.2-0.7-0.5-1.2c-0.3-0.5-0.4-1-0.6-1s-0.3,0.7-0.3,1.3c0,0.5,0,0.9,0.2,1c0.1,0,0.1,0.1,0.2,0.1c0-0.1,0-0.1,0-0.2c0-0.1,0-0.2,0-0.3c0-0.2,0-0.4,0-0.6c0.1,0.2,0.1,0.4,0.2,0.6c0,0.1,0.1,0.2,0.1,0.3c0,0.1,0.1,0.2,0.1,0.2c0.1,0,0.2,0,0.2,0v-0.1c0-0.1,0-0.2-0.1-0.2c0-0.2-0.1-0.3-0.1-0.5C29.1,63.7,29.2,63.8,29.3,64.1L29.3,64.1z M29.7,55.1c0-0.1-0.1-0.2-0.2-0.2c-0.1,0-0.1-0.1-0.2-0.1c0,0.1-0.1,0.1-0.1,0.2l-0.2,0.2c-0.1,0.2-0.2,0.2-0.2,0.4c0-0.2,0-0.3,0.1-0.5c0-0.1,0-0.2,0.1-0.2v-0.1c-0.1,0-0.2,0-0.2,0h-0.1c0,0,0,0.1-0.1,0.1c0,0,0,0.1-0.1,0.1c0,0.1-0.1,0.2-0.1,0.2c0,0.1-0.1,0.2-0.1,0.2c0,0.1-0.1,0.2-0.1,0.2c0-0.1,0-0.2,0-0.2c0-0.1,0-0.2,0-0.2c0-0.1,0-0.2,0-0.2c-0.6,0.5-0.2,2.2,0.1,2.2c0.2,0,0.2-0.4,0.7-1C29.4,55.7,29.8,55.5,29.7,55.1z M23.9,53.8c0-0.1,0-0.2,0.1-0.3c0-0.1,0.1-0.2,0.1-0.2c-0.1-0.1-0.2-0.1-0.2-0.1c-0.5,0.5-0.3,2-0.1,2.7c0.2,0.7,0.3,0.5,0.7-0.1c0.3-0.6,0.7-0.8,0.7-1.2c0-0.1-0.1-0.2-0.2-0.3l-0.1,0.2c-0.1,0.1-0.2,0.2-0.2,0.3c0-0.2,0.1-0.2,0.1-0.4c0.1-0.2,0.1-0.2,0.2-0.4c0-0.1-0.1-0.1-0.2-0.2l0,0c-0.1,0.1-0.1,0.1-0.2,0.2s-0.2,0.2-0.2,0.3c0-0.2,0.1-0.3,0.1-0.5c0-0.1,0.1-0.2,0.1-0.2l0.1-0.1c-0.1-0.1-0.1-0.1-0.2-0.2c0,0.1-0.1,0.1-0.1,0.2c-0.1,0.1-0.1,0.2-0.2,0.2S24,53.9,24,54c0,0.1-0.1,0.2-0.1,0.2C24,54.1,24,54.1,23.9,53.8C23.9,54.1,23.9,53.9,23.9,53.8z M26.4,61.5c0.2,0.3,0.3,0.7,0.5,0.9c0.2,0.3,0.3,0.6,0.6,0.9c0.1,0.2,0.2,0.2,0.3,0.4c0,0.1,0.1,0.1,0.1,0.2v-0.1c0.1-0.5,0-0.7-1.2-2.6c-1.2-2.1-1.2-3.6-1.6-3.6c-0.4,0-0.7,2.5-0.4,4.6c0.2,1.4,0.6,2.1,1.1,2.4c-0.1-0.2-0.2-0.4-0.2-0.6c-0.1-0.3-0.2-0.7-0.2-1.1c-0.1-0.3-0.2-0.7-0.2-1.1c0-0.3-0.1-0.7-0.1-1.1c0.1,0.3,0.1,0.7,0.2,1.1c0.1,0.3,0.2,0.7,0.2,1.1c0.1,0.3,0.2,0.7,0.4,1c0.2,0.3,0.2,0.7,0.5,0.9c0.1,0,0.2,0,0.2,0h0.1c-0.1-0.2-0.2-0.2-0.2-0.4c-0.1-0.2-0.2-0.4-0.2-0.7c-0.2-0.4-0.3-0.9-0.4-1.3c-0.1-0.4-0.2-0.9-0.3-1.3c-0.1-0.2-0.1-0.5-0.2-0.7c0-0.2-0.1-0.5-0.1-0.7c0.1,0.2,0.1,0.4,0.2,0.7c0.1,0.2,0.1,0.4,0.2,0.7c0.2,0.4,0.2,0.9,0.4,1.3c0.2,0.4,0.3,0.8,0.6,1.2c0.1,0.2,0.2,0.4,0.3,0.6c0.1,0.2,0.2,0.3,0.2,0.5c0.2-0.1,0.3-0.2,0.4-0.4l-0.1-0.2c-0.1-0.1-0.1-0.2-0.2-0.2c-0.1-0.2-0.2-0.3-0.2-0.5c-0.2-0.3-0.3-0.7-0.5-1c-0.2-0.3-0.2-0.7-0.4-1c-0.1-0.2-0.1-0.3-0.2-0.5l-0.2-0.5l0.2,0.5C26.3,61.3,26.4,61.4,26.4,61.5z M33.3,64.1c0.1,0,0.2-0.1,0.2-0.2c0.3-0.5,0-2-0.2-2c-0.2,0-0.1,0.3-0.6,1.1c-0.2,0.4-0.5,0.6-0.5,0.8c0,0.1,0.1,0.2,0.2,0.2l0.5-0.9l-0.2,1c0.1,0,0.2,0,0.2,0l0,0c0,0,0.1-0.1,0.1-0.2c0-0.1,0.1-0.2,0.1-0.2c0.1-0.2,0.1-0.3,0.2-0.6c0,0.2,0,0.4,0,0.6C33.3,63.9,33.3,64,33.3,64.1z M35.3,55c0,0.1,0,0.2,0,0.2c0,0.1,0,0.2,0,0.2s0,0.2,0,0.2c0-0.1,0-0.2-0.1-0.2c0-0.1-0.1-0.2-0.1-0.2c0-0.1-0.1-0.2-0.1-0.2c0,0,0-0.1-0.1-0.1c0,0,0-0.1-0.1-0.1h-0.1c-0.1,0-0.2,0-0.2,0v0.1c0,0.1,0,0.2,0.1,0.2c0,0.2,0.1,0.3,0.1,0.5c-0.1-0.2-0.2-0.2-0.2-0.4c0-0.1-0.1-0.2-0.2-0.2c-0.1-0.1-0.1-0.1-0.1-0.2c-0.1,0-0.2,0.1-0.2,0.1c-0.1,0.1-0.1,0.1-0.2,0.2c-0.2,0.2,0.2,0.5,0.5,1c0.3,0.6,0.4,1,0.7,1C35.5,57.1,35.9,55.5,35.3,55z M29.9,33.3c0.2-1.1,0.7-2.2,1.6-2.4c-0.1-0.6-0.2-1.2-0.6-1.7c-0.1,0.1-0.2,0.2-0.4,0.2c-0.2,0.1-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5-0.1c-0.2-0.1-0.2-0.2-0.4-0.2c-0.2,0.4-0.3,0.8-0.4,1.2C29.5,31,29.8,32.3,29.9,33.3z M33.6,55.2c-0.1-0.1-0.2-0.2-0.2-0.2c0,0.1,0,0.1-0.1,0.2c0,0.1-0.1,0.2-0.1,0.2c-0.1,0.2-0.1,0.3-0.2,0.6c0-0.2-0.1-0.4-0.1-0.6c0-0.1,0-0.2,0-0.2c0-0.1,0-0.1,0-0.2c-0.1,0-0.2,0-0.3,0c0,0.1,0,0.2-0.1,0.2c0,0.1,0,0.2,0,0.2c0,0.2,0,0.3,0,0.5c-0.1-0.2-0.2-0.3-0.2-0.4s-0.1-0.2-0.1-0.2v-0.1c-0.1,0-0.2,0.1-0.2,0.2C32,55.5,32,55.5,32,55.5c-0.1,0.5,0.7,1.8,0.9,1.8c0.2,0,0.2-0.4,0.3-0.7C33.6,55.8,33.8,55.6,33.6,55.2z M51.1,49.3c0.1,0.4,0.1,0.8,0.2,1.2c1.3-0.4,1.3-2.2,1.3-2.2C52.3,48.7,51.9,49.2,51.1,49.3z M35.1,64.3c0.1,0,0.1,0,0.2-0.1c0.2-0.1,0.2-0.5,0.2-1c0-0.5-0.2-1.3-0.3-1.3s-0.2,0.4-0.6,1c-0.3,0.5-0.7,0.9-0.5,1.2c0.1,0.1,0.2,0.2,0.2,0.2l0.1-0.1c0.1-0.1,0.1-0.2,0.2-0.2c0.1-0.2,0.2-0.3,0.2-0.5c0,0.2-0.1,0.3-0.1,0.5c0,0.1,0,0.2-0.1,0.2v0.1c0.1,0,0.2,0,0.2,0c0-0.1,0.1-0.2,0.1-0.2c0-0.1,0.1-0.2,0.1-0.3c0.1-0.2,0.1-0.4,0.2-0.6c0,0.2,0,0.4,0,0.6c0,0.1,0,0.2,0,0.3L35.1,64.3L35.1,64.3zM52.4,46.3c0.5-1.4-0.2-2.7-0.2-2.7c-0.2,1.8-0.8,2.6-1.8,3c0.2,0.6,0.3,1.2,0.4,1.7C51.6,47.8,52.1,47.1,52.4,46.3z M49.5,43.8c0.2,0.6,0.3,1.2,0.6,1.7c0.3-0.2,0.6-0.4,0.8-0.7c0.2-0.2,0.3-0.6,0.4-1c0.2-1.2,1.3-2,1.3-2C50.7,41.9,50.7,43.2,49.5,43.8zM40.1,57.9c-1,2.7-0.2,8.5-3.8,7.4c-2.2-0.7-2.8-0.4-4.5-0.4s-2.3-0.3-4.5,0.4c-3.6,1.2-2.8-4.7-3.8-7.4c-0.2-0.4-0.4-0.9-0.7-1.5c-0.1,0.4-0.2,0.9-0.3,1.4c0.2,0.3,0.4,1.1,0.7,2.9c0.4,2.7,0.7,4.5,1.9,5.1c1.2,0.7,2.2,0.1,4.4-0.2c0.7-0.1,1.7,0.1,2.3,0.1c0.7,0,1.7-0.2,2.3-0.1c2.2,0.3,3.1,0.9,4.4,0.2c1.2-0.7,1.5-2.4,1.9-5.1c0.2-1.9,0.6-2.7,0.7-2.9c-0.1-0.6-0.2-1.1-0.3-1.4C40.5,57,40.3,57.5,40.1,57.9z M43.2,57c0,0.2-0.1,0.5-0.1,0.8c-0.1,0.5-0.2,1.1-0.2,1.6c0.1-0.5,0.1-1.1,0.2-1.6c0-0.2,0-0.5,0-0.8c0-0.2,0-0.2,0-0.4c-0.2-0.2-0.4-0.4-0.6-0.7c0,0.1,0,0.2,0,0.2c0,0.4,0,0.9,0,1.3c0-0.4-0.1-0.9-0.1-1.3c0-0.2-0.1-0.3-0.1-0.5c-0.2-0.4-0.5-0.9-0.7-1.4c0.1,0.7,0.2,1.4,0.2,1.4c-0.2-1.2-0.7-2.2-1.2-3.3c-0.1,0.2-0.2,0.2-0.3,0.4c0,0.2,0.7,1.2,1.3,3.7c0.8,3.2,0.5,5.2,0.7,5.2c0.2,0,1.3-1.6,1.7-4.6c-0.2-0.1-0.5-0.2-0.7-0.4C43.2,56.9,43.2,57,43.2,57z M27.3,55.1c0-0.1,0-0.1,0-0.2c-0.1,0-0.2-0.1-0.3-0.1c0,0.1-0.1,0.2-0.1,0.3c-0.1,0.2-0.1,0.2-0.2,0.4c0-0.2,0-0.3,0-0.4c0-0.1,0-0.2,0-0.3c-0.1,0-0.2,0-0.2,0c0,0.2-0.1,0.3-0.1,0.5c0,0.2,0,0.4,0,0.6c-0.1-0.2-0.1-0.4-0.2-0.6c0-0.2-0.1-0.2-0.1-0.4c-0.2,0.2-0.2,0.5-0.2,0.8c0,0.4,0.2,1.2,0.6,1.2c0.2,0,0.3-0.4,0.7-0.8c0.4-0.4,0.6-0.7,0.4-1l-0.1-0.1c0,0,0,0,0,0.1c0,0.1-0.1,0.1-0.1,0.2c-0.1,0.1-0.2,0.2-0.2,0.3c0-0.1,0.1-0.2,0.1-0.3C27.3,55.1,27.3,55.1,27.3,55.1z M47.4,57.9c0,0,1.7,0.3,2.2-0.3c0.2-0.3,0.6-0.5,1-0.6c0-0.4,0.1-0.9,0.2-1.3v0.1C50.4,56.3,49.4,57.4,47.4,57.9z M31.5,63.8c0-0.2-0.2-0.4-0.5-0.8c-0.5-0.7-0.4-1.1-0.6-1.1c-0.2,0-0.7,1.5-0.2,2c0.1,0.1,0.1,0.1,0.2,0.2c0-0.1,0-0.2,0-0.3c0-0.2,0-0.4,0-0.6c0,0.2,0.1,0.4,0.2,0.6c0,0.1,0.1,0.2,0.1,0.2c0,0.1,0,0.1,0.1,0.2l0,0c0.1,0,0.2,0,0.2,0l-0.2-1l0.5,0.9C31.4,64,31.5,63.9,31.5,63.8z M23.5,39c0,0-1.1,0.1-1.6,0.7c0,0,2.2-0.6,3.2,0.2c1.1,0.7,1.7,0.7,1.7,0.7c-0.1-0.2-0.2-0.3-0.2-0.5c-0.2-0.4-0.8-0.8-0.8-0.8s0.9-0.1,1.5,1.1c0.6,1.1,1.2,0.9,1.2,0.9c-0.2-0.1-0.3-0.2-0.5-0.3c-0.2-0.2-0.2-0.3-0.2-0.6c0,0,0.7,0.7,1.6,0.7c0.2,0,0.5,0,0.7-0.2c0.2-0.1,0.4-0.2,0.5-0.5c-0.7-0.1-1-0.8-1.4-1.5c-0.2-0.2-0.3-0.4-0.5-0.5c-0.2-0.2-0.4-0.2-0.7-0.2c0.3,0.2,0.6,0.6,0.7,1c0.1,0.4,0.3,0.7,0.7,1.1c-1.1-0.2-1.8-1.7-2.2-2.3c-0.2-0.2-0.3-0.6-0.6-0.7c-0.2-0.2-0.5-0.3-0.8-0.4c0.3,0.3,0.6,0.7,0.7,1c0.2,0.3,0.2,0.7,0.2,1.2c-0.2-0.7-0.8-1.3-1.4-1.7c-0.6-0.4-1.2-0.9-1.5-1.6c0.1,0.3,0.2,0.7,0.3,1c0.2,0.3,0.5,0.7,0.7,1c-0.7-0.1-1.5-0.3-2.2-0.7c0.2,0.5,0.6,0.7,1.2,1.1c0.7,0.5,0.7,1.1,0.7,1.1s-0.9-1-2.2-1c-1.2,0-1.8-0.2-1.9-0.3c0.2,0.6,0.6,0.8,1.6,0.9C23.4,38.7,23.5,39,23.5,39z M22.7,44c0.3,0.2,0.8,0.1,0.5-0.2c-0.2-0.2-0.5-0.4-0.7-0.7c-0.2-0.3-0.2-0.6-0.2-0.9c0-0.2,0.1-0.5,0.2-0.7c0.1-0.2,0.2-0.5,0.4-0.7c0.2-0.2,0-0.4-0.2-0.2c-0.3,0.2-0.6,0.4-0.7,0.7c-0.2,0.3-0.2,0.7-0.2,1s0.1,0.7,0.3,1C22.1,43.6,22.4,43.9,22.7,44L22.7,44z M19.9,43.2c0.1,0.5,0.2,1,0.6,1.4c0.3,0.4,0.8,0.7,1.3,0.7c1.1,0.2,2.3-0.7,1.7-0.7l0,0c-1.4-0.2-2.5-1.2-2.5-2.4c0-0.5,0.2-1,0.5-1.4c0.2-0.2-0.1-0.4-0.6,0.1C20.5,41.5,19.9,42.1,19.9,43.2z M23.4,35.1c-0.7-0.1-0.9-1-0.9-1c-0.3,0.5-0.4,1.1-0.2,1.7c0.5,0.4,1.1,0.7,1.7,1C23.5,36.3,23.4,35.7,23.4,35.1z M27.1,28.5c-0.1,0.1-0.2,0.2-0.4,0.2c-0.2,0.1-0.3,0.1-0.5,0.1c-0.1,0-0.2,0-0.3,0c0.3,0.6,0.7,1.2,0.9,1.8c0.2-0.2,0.5-0.4,0.7-0.4C27.6,29.6,27.4,29.1,27.1,28.5z M16.3,37.8c1.1,0.1,1.6,0.6,1.7,0.7c-0.3-0.9-0.9-1.7-1.7-2.2c0,0,2.4,0.2,2.6,4.2c0,0-0.7,0.4-0.7,0.7c0,0.3,0.9,1.4,1.1,1c0.2-0.3,0.3-0.7,0.6-1.1c-0.2-1-1.1-5.7-4.6-5.7c0.2,0.3,0.3,0.7,0.5,1.1C16.1,37,16.2,37.4,16.3,37.8z M25.7,54.2c0.4,0.3,2,0.2,6.2,0.2c4.1,0,5.7,0.1,6.2-0.2c0.3-0.2,1.2-1.4,2.2-2.9c-0.2-0.2-0.2-0.2-0.5-0.3c-0.7-0.2-1.2,1.3-2,1.8c-0.8,0.5-2.7,0.3-5.9,0.3c-3.3,0-5,0.2-5.9-0.3s-1.3-2.1-2-1.8c-0.2,0.1-0.3,0.2-0.5,0.2C24.5,52.8,25.4,54,25.7,54.2z M22.5,33.2c-0.9-0.3-1.7-0.7-2.5-1.2c-0.6-0.4-1-1-1.2-1.7c-0.1,0.8-0.1,1.7,0.1,2.6c0.1,0,0.1,0.1,0.2,0.1c0.2-0.2,0.2-0.2,0.4-0.3s0.3-0.1,0.6-0.1h0.1c0.8,0.1,1.3,0.8,1.7,1.8C21.9,33.9,22.1,33.6,22.5,33.2z M28.9,31.4c-0.1,0.1-0.2,0.2-0.4,0.2c-0.2,0.1-0.3,0.1-0.5,0.1c-0.2,0-0.5-0.1-0.7-0.2c0.1,0.2,0.2,0.6,0.3,0.8c0.2,0.5,0.4,0.9,0.7,1.3c0.2-0.2,0.2-0.2,0.5-0.3s0.4-0.2,0.6-0.2C29.3,32.6,29.1,32,28.9,31.4z M38.3,63c-0.1,0.3-0.2,0.7-0.2,1.1c-0.1,0.2-0.2,0.4-0.2,0.6c0.5-0.3,0.8-1,1.1-2.4c0.3-2.1-0.1-4.6-0.4-4.6s-0.4,1.6-1.6,3.6c-1.1,2-1.2,2.1-1.2,2.6v0.1l0.1-0.2c0.1-0.2,0.2-0.2,0.3-0.4c0.2-0.2,0.4-0.6,0.6-0.9c0.2-0.3,0.3-0.6,0.5-0.9c0.1-0.2,0.2-0.3,0.2-0.5l0.2-0.5L37.6,61c-0.1,0.2-0.1,0.3-0.2,0.5c-0.2,0.3-0.2,0.7-0.4,1c-0.2,0.3-0.3,0.7-0.5,1c-0.1,0.2-0.2,0.3-0.2,0.5c-0.1,0.1-0.1,0.2-0.2,0.2L36,64.4c0.1,0.2,0.2,0.3,0.4,0.4c0.1-0.2,0.2-0.3,0.2-0.5s0.2-0.4,0.3-0.6c0.2-0.4,0.4-0.8,0.6-1.2c0.2-0.4,0.3-0.8,0.4-1.3c0.1-0.2,0.2-0.4,0.2-0.7c0.1-0.2,0.1-0.4,0.2-0.7c0,0.2-0.1,0.5-0.1,0.7c0,0.2-0.1,0.5-0.2,0.7c-0.1,0.5-0.2,0.9-0.3,1.3c-0.2,0.4-0.2,0.9-0.4,1.3c-0.1,0.2-0.2,0.4-0.2,0.7c-0.1,0.2-0.1,0.2-0.2,0.4H37c0.1,0,0.2,0,0.2,0c0.2-0.3,0.3-0.6,0.5-0.9c0.2-0.3,0.2-0.7,0.4-1c0.1-0.3,0.2-0.7,0.2-1.1c0.1-0.3,0.2-0.7,0.2-1.1c0,0.3,0,0.7-0.1,1.1C38.4,62.3,38.4,62.6,38.3,63z M23.2,52.3c-0.5,1.1-0.9,2.2-1.2,3.3c0,0,0-0.7,0.2-1.4c-0.2,0.5-0.4,1-0.7,1.4c0,0.2-0.1,0.3-0.1,0.5c-0.1,0.4-0.1,0.9-0.1,1.3c0-0.4,0-0.9,0-1.3v-0.2c-0.2,0.2-0.4,0.4-0.6,0.7c0,0.2,0,0.2,0,0.4c0,0.2,0,0.5,0,0.8c0,0.5,0.1,1.1,0.2,1.6c-0.1-0.5-0.2-1.1-0.2-1.6c0-0.2-0.1-0.5-0.1-0.8c0-0.1,0-0.2,0-0.2c-0.2,0.2-0.4,0.3-0.7,0.4c0.3,2.9,1.5,4.6,1.7,4.6c0.2,0-0.1-2.1,0.7-5.2c0.7-2.5,1.3-3.6,1.3-3.7C23.4,52.6,23.3,52.5,23.2,52.3z M12.4,43.9c0.1,0.3,0.2,0.7,0.4,1c0.2,0.2,0.5,0.5,0.8,0.7c0.2-0.6,0.3-1.2,0.6-1.7c-1.2-0.6-1.2-1.8-3.1-1.9C11.1,41.9,12.2,42.5,12.4,43.9z M40,55.8c0.2-0.7,0.5-2.1-0.1-2.7c0-0.1-0.2,0-0.2,0.1c0,0.1,0.1,0.2,0.1,0.2c0,0.1,0.1,0.2,0.1,0.3c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.3c0-0.1-0.1-0.2-0.1-0.2c0-0.1-0.1-0.2-0.1-0.2c-0.1-0.1-0.1-0.2-0.2-0.2c0-0.1-0.1-0.1-0.1-0.2c-0.1,0.1-0.1,0.1-0.2,0.2c0,0,0,0.1,0.1,0.1c0,0.1,0.1,0.2,0.1,0.2c0.1,0.2,0.1,0.3,0.1,0.4c-0.1-0.2-0.2-0.2-0.2-0.3L39,53.9l0,0l-0.2,0.2c0.1,0.2,0.1,0.2,0.2,0.4c0.1,0.2,0.1,0.2,0.1,0.4c-0.1-0.1-0.2-0.2-0.2-0.3c-0.1-0.1-0.1-0.2-0.2-0.2c-0.1,0.1-0.2,0.2-0.2,0.3c0,0.3,0.5,0.6,0.7,1.2C39.6,56.3,39.7,56.5,40,55.8z M36.1,55.1L36.1,55.1C36.1,55.1,36.1,55.1,36.1,55.1c-0.2,0.3,0,0.7,0.3,1.1c0.4,0.4,0.4,0.8,0.7,0.8c0.2,0,0.5-0.8,0.6-1.2c0-0.3,0.1-0.7-0.2-0.8c0,0.2-0.1,0.2-0.1,0.4c0,0.2-0.1,0.4-0.2,0.6c0-0.2,0-0.4,0-0.6c0-0.2,0-0.3-0.1-0.5c-0.1,0-0.2,0-0.2,0c0,0.1,0,0.2,0,0.3c0,0.2,0,0.3,0,0.4c-0.1-0.2-0.1-0.2-0.2-0.4c0-0.1-0.1-0.2-0.1-0.3c-0.1,0-0.2,0-0.3,0.1c0,0.1,0,0.1,0,0.2s0,0.1,0,0.2c0,0.2,0.1,0.2,0.1,0.3c-0.1-0.1-0.2-0.2-0.2-0.3C36.2,55.2,36.2,55.2,36.1,55.1z M11.1,48.3c0,0,0.1,1.8,1.3,2.2c0-0.4,0.1-0.8,0.2-1.2C11.8,49.2,11.4,48.7,11.1,48.3z M11.4,43.6c0,0-0.7,1.3-0.2,2.7c0.3,0.8,0.8,1.5,1.5,2c0.2-0.6,0.2-1.2,0.4-1.7C12.3,46.2,11.7,45.4,11.4,43.6z M31.8,16.1C14.3,16.1,0,30.3,0,47.8s14.3,31.8,31.8,31.8s31.8-14.3,31.8-31.8S49.2,16.1,31.8,16.1z M52.1,39c-1.2,1.8-2.3,1.6-2.3,1.6c1.1-0.6,1.6-1.7,1.8-2.6C51.8,38.4,52,38.7,52.1,39z M41.2,28.1c-0.2,0.4-0.6,0.8-1,1.2c0.2-0.4,0.5-0.9,0.7-1.3L41.2,28.1zM40.5,27.8c-0.2,0.3-0.6,0.7-0.8,1.1c0.2-0.4,0.3-0.8,0.6-1.2L40.5,27.8z M23.5,27.6c0.2,0.4,0.4,0.7,0.6,1.2c-0.2-0.3-0.6-0.7-0.8-1.1C23.3,27.7,23.4,27.7,23.5,27.6z M22.7,27.9c0.2,0.4,0.5,0.9,0.7,1.3c-0.3-0.3-0.7-0.7-1-1.2L22.7,27.9zM14.5,34.2L14.5,34.2c2.5-0.2,5,1,6,6.3c0.4-0.5,0.9-0.9,1.4-1.2c0,0-0.2,0-1.1-0.6c-0.7-0.6-0.7-2.4-0.7-2.4c0.2,0.4,0.7,0.8,1.1,1.1c0.4,0.2,0.8,0.2,1.2,0.2c-1.6-0.3-1.7-2.6-3.3-3.6c-0.8-0.6-2.5-0.7-3.7-0.7c0.2-0.2,0.5-0.5,0.7-0.8c0.5,0,1,0,1.5,0.1c-0.1-0.4-0.1-0.9-0.2-1.3c0.8-0.7,1.8-1.4,2.7-2c0.2,0.3,0.6,0.6,0.9,0.8c0.1-0.4,0-0.8-0.2-1.2c0.3-0.2,0.7-0.4,1.1-0.6c0.4,0.7,1.2,1.8,1.6,2.7c0.7,1.2-0.9,0.8-2.1,0.3c-0.7-0.2-1.3-0.7-1.7-1.2c0,0,0.3,1.2,2.2,1.7s2,0.9,2,0.9c-0.7-0.3-1.3-0.6-2.1-0.7c-0.7-0.1-1.4-0.4-1.9-0.9c1,1.4,3.2,1.6,4.1,2.3c0.8,0.7,1.3,2.2,1.5,2.5c0.2,0.2,0.2-0.2-0.2-0.9c-0.3-0.7-0.7-1.9-0.7-1.9s0.7,1.5,1.5,2.7c1,1.6,1.6,2,1.5,1.7c-0.1-0.2-1.2-1.7-1.5-2.7c-0.4-1.2-1.2-3-1.2-3c1,1.2,1,2.2,1.9,3.7s1.6,2.6,1.8,2.5c0.2-0.1-0.6-1.1-1-1.7c-0.7-1-1.1-2-1.4-3.1c-0.3-1-1.2-2.7-1.2-2.7c0.7,0.7,1.2,1.6,1.8,3.3c0.6,1.7,1,1.9,1.7,3s0.5,0.2,0.2-0.2c-0.2-0.4-1.8-2.9-2.6-5.1c-0.5-1.4-1.6-2.9-2.2-3.7c0.2-0.1,0.6-0.2,0.8-0.3c0.2,0.2,0.3,0.4,0.5,0.6c0.2-0.2,0.3-0.2,0.5-0.3c0.2-0.1,0.4-0.2,0.6-0.2h0.1c0-0.2-0.1-0.3-0.2-0.6c0.2-0.1,0.4-0.1,0.6-0.2c0.1,0.2,0.1,0.5,0.2,0.7c0.1-0.2,0.1-0.6,0.2-0.8c0.2-0.1,0.5-0.1,0.7-0.2c-0.2,0.5-0.3,1-0.4,1.5c0.5,0.6,0.7,1.5,0.8,2.3c0.2-1.1,0.7-2.2,1.7-2.2h0.1h0.1c-0.1-0.6-0.2-1.2-0.5-1.7l0,0V26c0.4,0,0.8-0.1,1.3-0.1c-0.2,0.2-0.2,0.6-0.3,0.8c0.1,0.4,0.2,0.8,0.2,1.2c0.5,0.3,0.8,0.9,1.1,1.7c0.2-0.7,0.5-1.3,1.1-1.7c0-0.4,0.1-0.8,0.2-1.2c-0.1-0.3-0.2-0.6-0.3-0.8c0.4,0,0.8,0.1,1.2,0.1c0,0,0,0,0,0.1l0,0c-0.2,0.6-0.4,1.2-0.6,1.7h0.1h0.1c1,0.1,1.5,1.2,1.7,2.2c0.1-0.8,0.3-1.7,0.8-2.3c-0.1-0.5-0.2-1-0.4-1.5c0.2,0.1,0.5,0.1,0.7,0.2c0.1,0.2,0.2,0.6,0.2,0.8c0-0.2,0.1-0.5,0.2-0.7c0.2,0.1,0.4,0.1,0.6,0.2c-0.1,0.2-0.1,0.4-0.1,0.5h0.1c0.2,0,0.4,0.1,0.6,0.2c0.2,0.1,0.3,0.2,0.5,0.3c0.2-0.2,0.3-0.4,0.5-0.6c0.2,0.1,0.6,0.2,0.8,0.3c-0.7,0.8-1.7,2.3-2.2,3.7c-0.7,2.2-2.3,4.6-2.6,5.1c-0.2,0.4-0.4,1.4,0.2,0.2c0.7-1.1,1.1-1.3,1.7-3c0.7-1.7,1.1-2.6,1.8-3.3c0,0-1,1.7-1.2,2.7c-0.3,1.1-0.8,2.2-1.4,3.1c-0.4,0.7-1.2,1.7-1,1.7c0.2,0.1,0.9-0.9,1.8-2.5c0.8-1.6,0.8-2.5,1.9-3.7c0,0-0.7,1.8-1.2,3c-0.3,1-1.4,2.4-1.5,2.7c-0.1,0.2,0.5-0.2,1.5-1.7c0.7-1.2,1.5-2.7,1.5-2.7s-0.4,1.2-0.7,1.9c-0.3,0.7-0.3,1.2-0.2,0.9c0.2-0.3,0.7-1.8,1.5-2.5c0.8-0.7,3.1-0.9,4.1-2.3c-0.6,0.5-1.2,0.8-2,1c-0.7,0.1-1.4,0.3-2.1,0.7c0,0,0.1-0.5,2-0.9c1.9-0.5,2.2-1.7,2.2-1.7c-0.5,0.6-1.1,1-1.7,1.2c-1.2,0.5-2.7,0.8-2.1-0.3c0.4-0.8,1.2-1.9,1.6-2.6c0.3,0.2,0.7,0.3,1.1,0.6c-0.2,0.4-0.2,0.8-0.2,1.2c0.3-0.2,0.7-0.5,0.9-0.7c1,0.6,1.9,1.2,2.7,2c0,0.4-0.1,0.8-0.2,1.2c0.5-0.1,0.9-0.1,1.4-0.1c0.2,0.2,0.5,0.5,0.7,0.8c-1.2,0-2.7,0.2-3.7,0.7c-1.6,1-1.7,3.2-3.3,3.6c0.4,0.1,0.9,0,1.2-0.2c0.4-0.2,0.8-0.6,1.1-1.1c0,0,0,1.8-0.7,2.4c-0.7,0.6-1.1,0.6-1.1,0.6c0.5,0.3,1,0.7,1.4,1.2c1-5.3,3.5-6.5,5.9-6.3l0,0c0.1,0.2,0.2,0.2,0.3,0.4c-0.5,0.6-0.9,1.2-1.2,2c-0.7,2.1-1,4.1-0.2,7c0.7,2.3,2,5.5,1.7,8.5c-0.4,3.1-2.2,4.4-4.6,3.7c-2.4-0.7-2.7-5.2-4.1-6.1c-1.3-0.8-2-0.2-2.5,0.7c-0.5,1-1.3,1.7-2.2,1.7c-1.4,0-1.7-1.1-1.7-1.1c-0.5,1.2-1.5,1.5-2.7,1.5c-1.2,0-2.2-0.2-2.7-1.5c0,0-0.2,1.1-1.7,1.1c-1,0-1.8-0.7-2.2-1.7c-0.4-0.8-1.1-1.5-2.5-0.7c-1.5,0.9-1.7,5.4-4.1,6.1c-2.4,0.7-4.2-0.7-4.6-3.7c-0.4-3.1,1-6.2,1.7-8.5c0.8-2.9,0.6-4.9-0.2-7c-0.4-1.1-0.8-1.7-1.2-2C14.3,34.5,14.4,34.3,14.5,34.2L14.5,34.2z M11.9,38c0.2,0.9,0.7,2.1,1.9,2.7c0,0-1.2,0.2-2.3-1.7C11.7,38.6,11.9,38.3,11.9,38L11.9,38z M31.6,70.3c-0.2,0-0.2,0-0.4,0c0.2-0.2,0.3-0.3,0.4-0.6C31.7,70,31.7,70.1,31.6,70.3L31.6,70.3z M32.1,70.3c0-0.2-0.1-0.3-0.1-0.6c0.1,0.2,0.2,0.4,0.4,0.6H32.1z M54,50.9c-0.1,0.5-0.2,1-0.2,1.5c-0.2-0.1-0.3-0.2-0.4-0.2c-0.2,0.5-0.4,1-0.5,1.6c0.2,0,0.4,0.1,0.6,0.2c-0.2,0.4-0.2,0.9-0.4,1.3c-0.1,0-0.2,0.1-0.3,0.1c-0.2,0-0.3,0-0.5-0.1s-0.2-0.2-0.4-0.2c-0.3,0.6-0.5,1.2-0.6,2c0.3,0.1,0.7,0.3,0.8,0.6c-0.1,0.2-0.2,0.4-0.3,0.7l0,0c-0.1,0.1-0.2,0.2-0.4,0.2c-0.2,0.1-0.3,0.1-0.5,0.1s-0.3,0-0.5-0.1c-0.2-0.1-0.2-0.2-0.4-0.2c-0.2,0.6-0.4,1.2-0.6,1.7c0.3,0.2,0.6,0.5,0.7,0.8c-0.2,0.2-0.2,0.4-0.4,0.6c-0.1-0.1-0.1-0.2-0.2-0.3c-0.2,0.2-0.5,0.3-0.8,0.3s-0.7-0.2-0.8-0.3c-0.2,0.6-0.5,1.2-0.6,1.9c0.2,0.1,0.5,0.2,0.7,0.5c-0.3,0.3-0.7,0.7-1.1,1h-0.1c-0.2,0-0.3,0-0.5-0.1c-0.2-0.1-0.2-0.2-0.4-0.2c-0.2,0.5-0.4,1.1-0.5,1.6c-2.5,1.9-5.4,3.3-8.5,4.1c-0.2-0.7-1.2-1.1-1.2-1.1c-1.2,0.2-2.2-0.9-2.2-0.9c0.2,1.1,1.7,1.2,1.7,1.2C35,69.6,34,70.3,34,70.3c-0.2,0-0.5,0-0.7,0.1c-0.3,0-0.6,0-0.9,0.1c-0.2-0.4-0.2-0.9-0.1-1.3c0.2,0.3,0.6,0.6,1,0.7c0.6,0.1,1.2-0.5,1.2-0.5c-0.5,0-0.9-0.2-1.2-0.6c-0.2-0.2-0.3-0.4-0.3-0.7c-0.1-0.2-0.1-0.5,0-0.7c0.9-0.4,2.1,1,2.1,1c-0.1-0.4-0.2-0.8-0.5-1.2c0.8,0.1,1.1,1.3,2.1,1.6c0.5,0.2,0.9,0.4,1.2,0.7c0.1-0.5-0.2-0.7-0.7-1.1c-0.1-0.1-0.2-0.2-0.2-0.3c-0.1-0.2,0-0.2,0-0.4c0.4,0.3,1,0.6,1.5,0.5c0.6,0,1.1-0.2,1.4-0.7c0.2-0.3,0.7-0.5,1-0.6C39.9,66.5,39.6,67,39,67c0,0,0.7-0.6,1.3-0.7c1.1,0,2.2-0.4,3-1c1.4-0.9,1.2-2.6,1.5-3.6c0.2-1.1,1.8-2.1,1.8-2.1c-0.9,0.2-1.7,0.7-2.3,1.5c-0.9,1.2-0.9,2.5-1.6,3.3c-0.7,0.8-2.6,0.3-2.6,0.3S40,65.7,39,66.7c-1.2,1.1-2.7,0.6-3.9,0.2c-1.2-0.3-2.3,0-3.3,0s-2.2-0.3-3.3,0c-1.2,0.3-2.7,0.9-3.9-0.2c-1.1-1-1.2-1.9-1.2-1.9s-1.9,0.5-2.6-0.3c-0.7-0.8-0.7-2.2-1.6-3.3c-0.7-0.7-1.4-1.2-2.3-1.5c0,0,1.6,1,1.8,2.1c0.2,1.1,0.1,2.7,1.5,3.6c0.9,0.7,1.9,1,3,1c0.6,0,1.3,0.7,1.3,0.7c-0.6,0.1-0.8-0.5-1.7-0.1c0.4,0.1,0.7,0.2,1,0.6c0.4,0.4,0.9,0.7,1.4,0.7c0.6,0,1.1-0.2,1.5-0.5c0.1,0.2,0.1,0.2,0,0.4c-0.1,0.2-0.2,0.2-0.2,0.3c-0.5,0.3-0.7,0.6-0.7,1.1c0.3-0.3,0.7-0.6,1.2-0.7c1-0.2,1.2-1.5,2.1-1.6c-0.2,0.3-0.4,0.7-0.5,1.2c0,0,1.2-1.5,2.1-1c0.1,0.2,0.1,0.5,0,0.7c-0.1,0.2-0.2,0.5-0.3,0.7c-0.3,0.3-0.7,0.6-1.2,0.6c0,0,0.5,0.5,1.2,0.5c0.4-0.1,0.7-0.3,1-0.7c0.1,0.4,0.1,0.9-0.1,1.3c-0.3,0-0.6,0-0.8,0c-0.2-0.1-0.5-0.2-0.7-0.1c0,0-1-0.7-1.2-1.2c0,0,1.5-0.2,1.7-1.2c0,0-1,1.1-2.2,0.9c0,0-1,0.4-1.2,1.1c-3.1-0.7-6.1-2.1-8.5-4.1h0.1c-0.1-0.6-0.2-1.2-0.5-1.7c-0.1,0.1-0.2,0.2-0.4,0.2c-0.2,0.1-0.3,0.1-0.5,0.1c-0.1,0-0.2,0-0.3,0c-0.3-0.2-0.6-0.6-0.9-0.8c0.2-0.2,0.5-0.5,0.8-0.7c-0.1-0.7-0.2-1.2-0.6-1.9c-0.1,0.1-0.2,0.2-0.4,0.2c-0.2,0-0.3,0-0.5,0c-0.2,0-0.3,0-0.5-0.1c-0.2-0.1-0.2-0.2-0.4-0.2c-0.1,0.2-0.2,0.3-0.2,0.5c-0.2-0.2-0.3-0.4-0.4-0.6c0.2-0.4,0.4-0.7,0.7-1c-0.1-0.6-0.2-1.2-0.6-1.7c-0.2,0.2-0.5,0.3-0.8,0.3c-0.3,0-0.7-0.2-0.8-0.3c0,0.1-0.1,0.2-0.1,0.2c-0.1-0.2-0.2-0.4-0.3-0.7c0.2-0.3,0.6-0.7,1-0.7c-0.1-0.7-0.2-1.3-0.6-2c-0.1,0.1-0.2,0.2-0.4,0.2c-0.2,0.1-0.3,0.1-0.5,0.1c-0.2,0-0.4-0.1-0.6-0.2c-0.1-0.2-0.2-0.4-0.2-0.7c-0.1-0.2-0.1-0.3-0.2-0.5c0.2-0.2,0.4-0.3,0.7-0.3c-0.1-0.6-0.2-1.1-0.5-1.6c-0.2,0.2-0.3,0.2-0.6,0.3c-0.1-0.5-0.2-1.1-0.2-1.6h0.1c0.3,0,0.6,0.2,0.8,0.3c-0.1-0.2-0.2-0.4-0.2-0.7c-0.3-0.9-0.5-1.8-0.7-2.8c0.1-2.7,0.7-5.5,1.7-8c0.2,0.4,0.5,0.7,0.8,1.1c0.9,0.7,2.3,1.3,2.6,1.4c0.3-1.2,0.4-2.6,0.2-3.9c-1.8-0.1-2.2-1.6-2.2-1.6c0.6,0.6,1.2,0.9,2.1,1c-0.1-0.4-0.2-0.8-0.3-1.2c-0.2-0.5-0.4-1-0.7-1.5c0.1-0.2,0.2-0.2,0.2-0.4c0.4,0.7,0.7,1.3,0.9,2.1c0.5,1.7,0.7,4.3-0.2,6.7c-0.8,2.6-2.5,7-1.5,9.9c0.9,3,3.2,4.1,5.6,3.4c2.2-0.7,2.8-2.8,3.1-3.9c0.2-1.1,1.1-2.7,2.2-2.7c0.8,0,0.9,0.7,1.7,1.6c0.6,0.6,1.4,0.8,2.2,0.8c0.4,0,2.7,0.1,3.8,0.1s3.4-0.1,3.8-0.1c0.8,0,1.6-0.2,2.2-0.8c0.9-0.7,0.9-1.6,1.7-1.6c1.1,0.1,1.9,1.6,2.2,2.7s0.7,3.2,3.1,3.9c2.4,0.7,4.6-0.3,5.6-3.4c0.9-3-0.7-7.5-1.5-9.9c-0.8-2.4-0.7-5.1-0.2-6.7c0.2-0.7,0.5-1.3,0.8-2c0.1,0.2,0.2,0.2,0.2,0.4c-0.2,0.5-0.5,0.9-0.6,1.4c-0.1,0.3-0.2,0.7-0.3,1.2c0.7-0.1,1.5-0.4,2-0.9l0,0c-0.1,0.2-0.5,1.4-2.2,1.5c-0.2,1.3-0.1,2.7,0.2,3.9c0.2-0.1,1.7-0.7,2.6-1.4c0.3-0.2,0.6-0.7,0.8-1c1.1,2.7,1.7,5.5,1.7,8.3c-0.2,0.8-0.4,1.6-0.7,2.4c-0.1,0.2-0.2,0.5-0.2,0.7C53.5,51.1,53.7,51,54,50.9L54,50.9z M40.1,41.9c0-0.2-0.1-0.5-0.2-0.7c-0.2-0.2-0.3-0.3-0.5-0.4c-0.2-0.1-0.4-0.1-0.7-0.1c-0.2,0.1-0.4,0.2-0.6,0.3c-0.2,0.2-0.2,0.3-0.3,0.6c-0.1,0.2,0,0.5,0.1,0.7c0.1,0.2,0.2,0.4,0.4,0.5c0.2,0.2,0.4,0.2,0.7,0.2c0.3,0,0.6-0.1,0.8-0.3C40,42.5,40.1,42.2,40.1,41.9z M38.2,41.9c0-0.2,0-0.2,0.1-0.4c0.1-0.1,0.2-0.2,0.3-0.2s0.2-0.1,0.4,0c0.2,0,0.2,0.1,0.3,0.2c0.1,0.1,0.2,0.2,0.2,0.3c0,0.2,0,0.2,0,0.4c-0.1,0.2-0.2,0.2-0.2,0.3c-0.1,0.1-0.2,0.1-0.4,0.1c-0.1,0-0.2,0-0.2-0.1c-0.1,0-0.2-0.1-0.2-0.2c-0.1-0.1-0.1-0.2-0.2-0.2C38.3,42,38.2,42,38.2,41.9z M25.9,41.9c0-0.2-0.1-0.5-0.2-0.7c-0.2-0.2-0.3-0.3-0.5-0.4c-0.2-0.1-0.4-0.1-0.7-0.1c-0.2,0.1-0.4,0.2-0.6,0.3c-0.2,0.2-0.2,0.3-0.3,0.6c-0.1,0.2,0,0.5,0.1,0.7c0.1,0.2,0.2,0.4,0.4,0.5c0.2,0.2,0.4,0.2,0.7,0.2c0.2,0,0.3,0,0.4-0.1c0.2-0.1,0.2-0.2,0.4-0.2c0.1-0.1,0.2-0.2,0.2-0.4C25.9,42.2,25.9,42,25.9,41.9z M24,41.9c0-0.2,0-0.2,0.1-0.4c0.1-0.1,0.2-0.2,0.3-0.2c0.2-0.1,0.2-0.1,0.4,0c0.2,0,0.2,0.1,0.3,0.2s0.2,0.2,0.2,0.3c0,0.2,0,0.2,0,0.4c-0.1,0.2-0.2,0.2-0.2,0.3c-0.1,0.1-0.2,0.1-0.4,0.1c-0.2,0-0.3-0.1-0.5-0.2C24.1,42.2,24,42,24,41.9z"></path>
                    </svg>
                </div>
                <div id="locations-panel-list" class="locations-panel-list">
                    <header>
                        <h1 class="search-title">
                            <?php _e( 'Geben Sie Ihre Adresse ein', 'go_by_henssler' ); ?>
                        </h1>
                        <div class="form-field">
                            <span class="search-input-icon icon-pin-small"></span>
                            <div class="search-input form-field__control">
                                <input id="location-search-input" class="form-field__input" placeholder=" ">
                                <label for="location-search-input" class="form-field__label">
                                    <?php _e( 'Ort wählen', 'go_by_henssler' ); ?>
                                </label>
                                <div id="search-overlay-search" class="search-input-overlay search">
                                    <button id="location-search-button"><?php _e( 'Search', 'go_by_henssler' ); ?>
                                        <span class="icon-compass-arrow"></span>
                                    </button>
                                </div>
                                <div class="form-field__bar"></div>
                            </div>
                        </div>
                    </header>
                    <div class="section-name" id="location-results-section-name">
                        <?php _e( 'All locations', 'go_by_henssler' ); ?>
                    </div>
                    <div class="results">
                        <div class="location-results-hint">
                            <?php _e( 'Alle Fahrten liegen im Umkreis von 200 km zu dem von Ihnen angegebenen Standort.', 'go_by_henssler' ); ?>
                        </div>
                        <ul class="location-results-list" id="location-results-list"></ul>
                    </div>
                </div>
                <div id="locations-panel-details"></div>
            </div>
        </div>
    </div>

    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo esc_attr( $gm_api_key ); ?>&callback=initMap&libraries=places,geometry&solution_channel=GMP_QB_locatorplus_v4_cABCDE" async defer></script>

<?php get_footer('empty'); ?>
