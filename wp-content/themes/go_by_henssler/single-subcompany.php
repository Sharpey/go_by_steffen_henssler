<?php
/**
 * The Template for single subcompany
 */

defined( 'ABSPATH' ) || exit;

global $post;
global $post_meta;

$post_meta     = (object)get_post_meta( $post->ID );
$tour          = $_REQUEST['tour'] ?? 0;
$tour_date     = ! empty( $post_meta->{'tour_'.$tour.'_date'}[0] ) ? $post_meta->{'tour_'.$tour.'_date'}[0] : '';
$tour_active   = $post_meta->{'tour_'.$tour.'_active'}[0] ?? false;
$date_time     = new DateTime();
$time_zone     = new DateTimeZone( 'CET' );
$current_date  = $date_time->setTimezone( $time_zone )->format( 'Ymd' );
$current_time  = $date_time->setTimezone( $time_zone )->format( 'Hi' );
$cur_timestamp = $date_time->setTimezone( $time_zone )->format( 'U' );
$current_time  = $date_time->setTimezone( new DateTimeZone( 'CET' ) )->format( 'Hi' );
$tour_time     = ! empty( $post_meta->{'tour_'.$tour.'_time_to_accept_orders'}[0] ) ? $post_meta->{'tour_'.$tour.'_time_to_accept_orders'}[0] : '23:59:00';
$tour_time     = str_replace( ':', '', substr( $tour_time, 0, -3 ) );
$one_day_diff  = ( strtotime( $tour_date, $cur_timestamp ) - strtotime( $current_date, $cur_timestamp ) ) / 60 / 60 / 24 === 1;

if ( ! $tour_active || $tour_date <= $current_date || ( $one_day_diff && $tour_time <= $current_time ) ) {
    wp_redirect( home_url() );
}

if ( isset( $_SESSION['subcompany'] ) && isset( $_SESSION['tour'] ) ) {
    if ( $_SESSION['subcompany'] !== $post->ID ) {
        WC()->cart->empty_cart();
    } else if ( $_SESSION['tour'] !== $tour ) {
        WC()->cart->empty_cart();
    }
}

$_SESSION['subcompany'] = $post->ID;
$_SESSION['tour']       = $tour;

$product_list = [];
$exclude_list = [];

if ( ! empty( $post_meta->products[0] ) ) {
    $product_list = unserialize( $post_meta->products[0] );
}

if ( ! empty( $post_meta->{'tour_'.$tour.'_exclude_products'}[0] ) ) {
    $exclude_list = unserialize( $post_meta->{'tour_'.$tour.'_exclude_products'}[0] );
}

$_SESSION['full_address'] = $post_meta->full_address[0];

if ( ! empty( $post_meta->{'tour_'.$tour.'_address'}[0] ) ) {
    $_SESSION['tour_address'] = $post_meta->{'tour_'.$tour.'_address'}[0];
} else {
    $_SESSION['tour_address'] = $post_meta->full_address[0];
}

if ( ! empty( $post_meta->{'tour_'.$tour.'_start_time'}[0] ) &&
     ! empty( $post_meta->{'tour_'.$tour.'_end_time'}[0] ) )
{
    $_SESSION['tour_start_hr'] = $post_meta->{'tour_'.$tour.'_start_time'}[0];
    $_SESSION['tour_end_hr']   = $post_meta->{'tour_'.$tour.'_end_time'}[0];
}

if ( ! empty( $post_meta->{'tour_'.$tour.'_address'}[0] ) ) {
    $_SESSION['tour_address'] = $post_meta->{'tour_'.$tour.'_address'}[0];
} else {
    $_SESSION['tour_address'] = $post_meta->full_address[0];
}

if ( ! empty( $tour_date ) ) {
    $_SESSION['tour_date'] = gobh_convert_tour_date( $tour_date );
}

get_header();

$prod_cats = gobh_get_product_categories( array_diff( $product_list, $exclude_list ) );
?>

    <main id="primary" class="site-main">
        <div class="container-1376">

            <?php
            if ( ! empty( $prod_cats ) ) {
                get_template_part( 'template-parts/content', 'categories-list', $prod_cats );
            }
            ?>

            <div class="content-shop">
                <?php
                if ( empty( $prod_cats ) ) {
                    do_action( 'woocommerce_no_products_found' );
                } else {
                    foreach ( $prod_cats as $cat ) {
                        get_template_part( 'template-parts/content', 'shop-category', ['cat' => $cat, 'product_list' => $product_list, 'exclude_list' => $exclude_list] );
                    }
                }
                ?>
            </div>

        </div>
    </main><!-- #main -->

<?php get_template_part( 'template-parts/content', 'box-details' ); ?>
<?php get_template_part( 'template-parts/content', 'cart-details' ); ?>

<?php
get_footer();
