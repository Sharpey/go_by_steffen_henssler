const gulp = require('gulp');
const gulpStylelint = require('gulp-stylelint');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const autoprefixer = require('autoprefixer');
const rename = require('gulp-rename');
const babel = require('gulp-babel');
const minify = require('gulp-babel-minify');
const log = require('gulplog');
const tap = require('gulp-tap');
const browserify = require('browserify');
const buffer = require('gulp-buffer');
const esLint = require('gulp-eslint');
const svgmin = require('gulp-svgmin');
const size = require('gulp-filesize');

const postcss = require("gulp-postcss");
const cssnano = require("cssnano");
const mode = require('gulp-mode')();

const path = {
    srcStyles: 'src/sass',
    distStyles: 'dist/css',
    srcScripts: 'src/js',
    distScripts: 'dist/js',
    srcImg: 'src/img',
    distImg: 'dist/img'
};


/* ----------------------------- CSS tasks ---------------------------- */

gulp.task('style', () => {
    return gulp.src([
        `${path.srcStyles}/abstracts/*.scss`,
        `${path.srcStyles}/components/*.scss`,
        `${path.srcStyles}/global/*.scss`,
        `${path.srcStyles}/style.scss`,
        ])
        .pipe(gulpStylelint({
            reporters: [
                { formatter: 'string', console: true },
            ],
        }))
        .pipe(mode.development(sourcemaps.init()))
        .pipe(sass({ errLogToConsole: true }).on('error', sass.logError))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(mode.development(sourcemaps.write('./')))
        .pipe(gulp.dest(path.distStyles))
        .pipe(size());
});

gulp.task('sass', () => {
    return gulp.src([
        `${path.srcStyles}/*.scss`
        ])
        .pipe(sass({ errLogToConsole: true }).on('error', sass.logError))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(gulp.dest(path.distStyles))
        .pipe(size());
});

/* ----------------------------- JS tasks ----------------------------- */
gulp.task('js', () => {
    return gulp.src([
        `${path.srcScripts}/**/*.js`,
        `!${path.srcScripts}/**/*.min.js`,
        `!${path.srcScripts}/**/_*.js`
    ], { read: false })
    // transform file objects using gulp-tap plugin
        .pipe(tap((file) => {

            log.info(`bundling ${file.path}`);

            // replace file contents with browserify's bundle stream
            file.contents = browserify(file.path, { debug: true }).bundle();

        }))

        .pipe(rename({ extname: '.min.js' }))

        // transform streaming contents into buffer contents
        // (because gulp-sourcemaps does not support streaming contents)
        .pipe(buffer())

        // load and init sourcemaps
        .pipe(mode.development(sourcemaps.init({ loadMaps: true })))

        .pipe(babel({
            presets: ['@babel/preset-env'],
        }))
        .pipe(minify({
            mangle: {
                keepClassName: true,
            },
        }))

        // write sourcemaps
        .pipe(mode.development(sourcemaps.write('./')))
        .pipe(gulp.dest(path.distScripts))
        .pipe(size());
});


gulp.task('esLint', () => {
    return gulp.src([
        `${path.srcScripts}/**/*.js`,
        `!${path.srcScripts}/**/*.min.js`,
        ])
        .pipe(esLint({
            configFile: './.eslintrc.js',
        }))
        .pipe(esLint.format());
});

/* ----------------------------- SVG tasks ---------------------------- */
gulp.task('svg', () => {
    return gulp.src(`${path.srcImg}/svg/**/*.svg`)
        .pipe(svgmin())
        .pipe(gulp.dest(path.distImg))
        .pipe(size());
});

/* ----------------------------- Copy Fonts ---------------------------- */
gulp.task('fonts', () => {
    return gulp.src('./src/fonts/*')
        .pipe(gulp.dest('./dist/fonts'))
        .pipe(size());
});


/* -------------------------- WATCH, DEFAULT -------------------------- */

// Watch command
gulp.task('watch', () => {
    gulp.watch([`${path.srcStyles}/**/*.scss`], gulp.parallel('style', 'sass'));
    gulp.watch([`${path.srcScripts}/**/*.js`], gulp.series('esLint', 'js'));
    gulp.watch([`${path.srcImg}/svg/**/*.svg`], gulp.parallel('svg'));
});

// Build command
gulp.task('build', gulp.series(gulp.parallel('style', 'sass', 'svg', 'fonts', gulp.series('esLint', 'js')), 'watch'));

// Default
gulp.task('default', gulp.series('watch'));

