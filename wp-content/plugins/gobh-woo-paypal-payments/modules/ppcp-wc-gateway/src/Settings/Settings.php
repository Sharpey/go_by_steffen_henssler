<?php
/**
 * The settings object.
 *
 * @package WooCommerce\PayPalCommerce\WcGateway\Settings
 */

declare(strict_types=1);

namespace WooCommerce\PayPalCommerce\WcGateway\Settings;

use WooCommerce\PayPalCommerce\WcGateway\Exception\NotFoundException;
use Psr\Container\ContainerInterface;

/**
 * Class Settings
 */
class Settings implements ContainerInterface {

	const KEY = 'woocommerce-ppcp-settings';

	/**
	 * The settings.
	 *
	 * @var array
	 */
	private $settings = array();

	/**
	 * Returns the value for an id.
	 *
	 * @param string $id The value identificator.
	 *
	 * @return mixed
	 * @throws NotFoundException When nothing was found.
	 */
	public function get( $id ) {
		if ( ! $this->has( $id ) ) {
			throw new NotFoundException();
		}
		return $this->settings[ $id ];
	}

	/**
	 * Whether a value exists.
	 *
	 * @param string $id The value identificator.
	 *
	 * @return bool
	 */
	public function has( $id ) {
		$this->load();
		return array_key_exists( $id, $this->settings );
	}

	/**
	 * Sets a value.
	 *
	 * @param string $id The value identificator.
	 * @param mixed  $value The value.
	 */
	public function set( $id, $value ) {
		$this->load();
		$this->settings[ $id ] = $value;
	}

	/**
	 * Stores the settings to the database.
	 */
	public function persist() {

		return update_option( self::KEY, $this->settings );
	}

    /**
     * Custom filter to change the values of paypal settings.
     * This is required so each subcompany can use their own paypal settings
     *
     * @param $settings
     * @return mixed
     */
    public function gobh_filter_subcompany_paypal( $settings ){

        if ( ! empty( $_SESSION['subcompany'] ) ) {
            $subcompany_meta                       = get_post_meta( $_SESSION['subcompany'] );
            $settings['enabled']                   = true;
            $settings['sandbox_on']                = boolval( $subcompany_meta['sandbox_mode'][0] );
            $settings['merchant_email_sandbox']    = $subcompany_meta['sandbox_email'][0];
            $settings['merchant_email_production'] = $subcompany_meta['paypal_email'][0];
            $settings['merchant_id_sandbox']       = $subcompany_meta['sandbox_merchant_id'][0];
            $settings['merchant_id_production']    = $subcompany_meta['merchant_id'][0];
            $settings['client_id_sandbox']         = $subcompany_meta['sandbox_client_id'][0];
            $settings['client_id_production']      = $subcompany_meta['client_id'][0];
            $settings['client_secret_sandbox']     = $subcompany_meta['sandbox_secret_key'][0];
            $settings['client_secret_production']  = $subcompany_meta['secret_key'][0];

            if ( $settings['sandbox_on'] ) {
                $settings['merchant_email'] = $settings['merchant_email_sandbox'];
                $settings['merchant_id']    = $settings['merchant_id_sandbox'];
                $settings['client_id']      = $settings['client_id_sandbox'];
                $settings['client_secret']  = $settings['client_secret_sandbox'];
            } else {
                $settings['merchant_email'] = $settings['merchant_email_production'];
                $settings['merchant_id']    = $settings['merchant_id_production'];
                $settings['client_id']      = $settings['client_id_production'];
                $settings['client_secret']  = $settings['client_secret_production'];
            }
        }

        return $settings;
    }

	/**
	 * Loads the settings.
	 *
	 * @return bool
	 */
	private function load(): bool {

		if ( $this->settings ) {
			return false;
		}

		$this->settings = $this->gobh_filter_subcompany_paypal( get_option( self::KEY, array() ) );

		$defaults = array(
			'title'                         => __( 'PayPal', 'woocommerce-paypal-payments' ),
			'description'                   => __(
				'Pay via PayPal.',
				'woocommerce-paypal-payments'
			),
			'button_single_product_enabled' => true,
			'button_mini-cart_enabled'      => true,
			'button_cart_enabled'           => true,
			'brand_name'                    => get_bloginfo( 'name' ),
			'dcc_gateway_title'             => __( 'Credit Cards', 'woocommerce-paypal-payments' ),
			'dcc_gateway_description'       => __(
				'Pay with your credit card.',
				'woocommerce-paypal-payments'
			),
		);
		foreach ( $defaults as $key => $value ) {
			if ( isset( $this->settings[ $key ] ) ) {
				continue;
			}
			$this->settings[ $key ] = $value;
		}
		return true;
	}
}
